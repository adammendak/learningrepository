package com.adammendak.hibernate.Dao;

import com.adammendak.hibernate.model.Client;
import com.adammendak.hibernate.utils.HibernateUtil;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

public class ClientDaoImpl implements ClientDao {

    private Logger logger = Logger.getLogger(ClientDaoImpl.class);

    @Override
    public Client findById(Integer id) {
        SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        Client client = null;

        try {
            logger.info("trying to get client by id: " + id);
            client = session.load(Client.class, id);
            transaction.commit();

        } catch ( Exception e) {
            logger.error("Problem during loagin client wiht id : " + id,e);
            transaction.rollback();
        }
        finally {

        }

        return client;
    }

}
