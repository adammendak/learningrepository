package com.adammendak.hibernate.Dao;

import com.adammendak.hibernate.model.Client;

public interface ClientDao {

    public Client findById(Integer id);

}
