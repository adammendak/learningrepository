package com.adammendak.hibernate.model;

public enum ClientType {
    REGULAR, VIP, PREMIUM
}
