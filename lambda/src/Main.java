import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class Main {



    public static void main(String[] args) {

        final List<String> strings = Arrays.asList("a","b","c","d");

        strings.stream().forEach(s -> System.out.println(s));
        System.out.println("--------------------------");

//        List<String > upperStrings = new ArrayList<>();
//
//        strings.stream().forEach(System.out::println);
//        strings.stream().forEach(s -> upperStrings.add(s.toUpperCase()));
//        upperStrings.forEach(System.out::println);
//        strings.stream().map(s -> s.toUpperCase()).forEach(System.out::println);
        strings.stream().map(String::toUpperCase).forEach(System.out::println);

        System.out.println("----------------------------");

        List<String> aString = strings.stream().filter(s -> s.startsWith("a")).collect(Collectors.toList());
        aString.forEach(System.out::println);

        System.out.println("----------------------------");

        final Predicate<String> startsWithA = name -> name.startsWith("a");
        List<String> aSecondString = strings.stream().filter(startsWithA).collect(Collectors.toList());
        aSecondString.forEach(System.out::println);

        System.out.println("----------------------------");

        final Function<String, Predicate<String>> st = (String letter) -> {
            Predicate<String> criteria = (String name) -> name.startsWith(letter);
            return criteria;
        };

        List<String> aThirdString = strings.stream().filter(st.apply("a")).collect(Collectors.toList());
        aThirdString.forEach(System.out::println);



    }

    public static Predicate<String> criteria(String criteria) {
        return name -> name.startsWith(criteria);
    }
}
