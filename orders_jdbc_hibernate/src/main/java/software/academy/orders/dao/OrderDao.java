package software.academy.orders.dao;

import software.academy.orders.entities.Order;

public interface OrderDao {

    public void insert(Order order);

    // wyciaganie zamówienia z tabeli ORDER i przypisanie do obiektu Order
    // wyciaganie pozycji zamówienia z tabeli ORDER_ITEMS
    // w ramach pozycji zamówien chcemy informacje o produktucie nazwa itd (encja Product)
    public Order findById(Integer id);
}
