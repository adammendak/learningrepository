package software.academy.orders.entities;

import java.math.BigDecimal;

public class Product {

    Integer id;

    String name;

    BigDecimal price;
}
