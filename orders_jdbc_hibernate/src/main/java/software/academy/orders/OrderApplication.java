package software.academy.orders;

import software.academy.orders.dao.ClientDao;
import software.academy.orders.dao.ClientDaoImpl;
import software.academy.orders.entities.Client;

public class OrderApplication {

    public static void main(String[] args) {
        ClientDao clientDao = new ClientDaoImpl();
        Client client=clientDao.findById(2);
        System.out.println(client.getEmail());
    }
}
