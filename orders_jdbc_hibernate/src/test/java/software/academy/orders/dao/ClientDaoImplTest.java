package software.academy.orders.dao;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import software.academy.orders.entities.Client;
import software.academy.orders.entities.ClientType;
import software.academy.orders.exceptions.DatabaseException;

import static org.junit.Assert.*;

public class ClientDaoImplTest {

    @Test
    public void shouldFindById() throws Exception {
        ClientDao clientDao = new ClientDaoImpl();
        Client clientFromDatabase = clientDao.findById(1);
        Client expectedClient = new Client(1, "john", "bravo",
                "john@mailinator.com", ClientType.PREMIUM);
        Assert.assertEquals("Clients should be the same", expectedClient, clientFromDatabase);

    }

    @Test(expected = DatabaseException.class)
    public void shouldInsertClient() {
        ClientDao clientDao = new ClientDaoImpl();
        Client newClient = new Client("jan", "kowalski",
                "jan13@mailinator.com", ClientType.REGULAR);
        clientDao.insert(newClient);
    }

    @Test
    public void shouldUpdateClient() {
        ClientDao clientDao = new ClientDaoImpl();
        Client updatedClient = new Client(1,"johny", "rambo",
                "rambo@mailinator.com", ClientType.REGULAR);
        clientDao.update(updatedClient);
    }

    @Test
    @Ignore
    public void shouldDeleteClient() {
        ClientDao clientDao = new ClientDaoImpl();
        clientDao.delete(1);
    }

}