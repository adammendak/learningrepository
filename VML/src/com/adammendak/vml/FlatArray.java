package com.adammendak.vml;

import java.util.ArrayList;
import java.util.List;

public class FlatArray {

    public static void main(String[] args) {

        // TASK NUMBER 7 from the List

        Object[] array = {1, 2, new Object[]{3, 4, new Object[]{5}, 6, 7}, 8, 9, 10};
        List<Object> result = new ArrayList<>();
        boolean anyMoreObjects = true;

        for (int i = 0; i < array.length; i++) {
            result.add(array[i]);
        }

        List<Object> temp = new ArrayList<>();
        List<Object> objectsToRemove = new ArrayList<>();

        while (areAnyMoreObjects(result)) {

            for(int i =0 ; i<result.size(); i++) {
                if (result.get(i) instanceof Object[]) {
                    Object[] iCloned = ((Object[]) result.get(i)).clone();
                    for (Object clone : iCloned) {
                        temp.add(clone);
                    }
                    objectsToRemove.add(result.get(i));
                }
            }

            for(int i=0 ; i < objectsToRemove.size(); i++) {
                result.remove(objectsToRemove.get(i));
            }
            objectsToRemove.clear();
            result.addAll(temp);
            temp.clear();

        }
        
        System.out.println("Flatten result list:");
        result.forEach(System.out::println);
    }

    public static boolean areAnyMoreObjects (List<Object> listToCheck) {

        for (Object obj : listToCheck) {
            if(obj instanceof Object[]) {
                return true;
            }
        }
        return false;
    }

}
