package com.adammendak.vml;

import java.util.ArrayList;
import java.util.List;

public class PrimeNumbers {

    public static void main(String[] args) {

        //TASK NUMBER 6 from the LIST

        int[] array = {1, 2, 4, 6, 11, 13, 17, 8, 20, 32, 33, 42, 47, 53, 59};
        int[] array2 = {11, 17, 13, 5, 53, 8, 2, 35, 47, 84, 65, 151};

        int[] primeNumbersFirstArray = primeNumbers(array);
        int[] primeNumbersSecondArray = primeNumbers(array2);

        int[] commonPrimeNumbersArray = commonPrimeNumbers(primeNumbersFirstArray, primeNumbersSecondArray);
        commonPrimeNumbersArray = bubbleSort(commonPrimeNumbersArray);
        commonPrimeNumbersArray = addOneToEachInt(commonPrimeNumbersArray);

        for (int obj : commonPrimeNumbersArray) {
            System.out.println(obj);
        }

    }

    public static int[] primeNumbers(int[] array) {
        List<Integer> resultList = new ArrayList<>();
        List<Integer> listToCheck = new ArrayList<>();

        for (int i = 0; i < array.length; i++) {
            listToCheck.add(new Integer(array[i]));
        }

        listToCheck.stream()
                .forEach(integer -> {
                    if (integer == 1) {
                        resultList.add(integer);
                    } else if (integer == 2) {
                        resultList.add(integer);
                    } else {
                        if (isPrime(integer) == true) {
                            resultList.add(integer);
                        }
                    }
                });

        return resultList.stream().mapToInt(i->i).toArray();
    }

    public static boolean isPrime(int number) {
        for (int i = 2; i < number; i++) {
            if (number % i == 0) {
                return false;
            }
        }
        return true;
    }

    public static int[] commonPrimeNumbers(int[] array1, int[] array2) {

        List<Integer> resultList = new ArrayList<>();
        for (int i = 0; i < array1.length; i++) {
            for (int j = 0; j < array2.length; j++) {
                if (array1[i] == array2[j]) {
                    resultList.add(array1[i]);
                }
            }
        }
        return resultList.stream().mapToInt(i->i).toArray();
    }

    public static int[] bubbleSort(int[] array) {
        int n = array.length;
        int temp = 0;
        for (int i = 0; i < n; i++) {
            for (int j = 1; j < (n - i); j++) {
                if (array[j - 1] > array[j]) {
                    temp = array[j - 1];
                    array[j - 1] = array[j];
                    array[j] = temp;
                }
            }
        }
        return array;
    }

    public static int[] addOneToEachInt(int[] array) {
        for (int i = 0; i< array.length; i++) {
            array[i] = array[i] + 1;
        }
        return array;
    }

}
