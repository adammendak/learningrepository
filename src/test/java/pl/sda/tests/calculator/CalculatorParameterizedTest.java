package pl.sda.tests.calculator;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.lang.reflect.Array;
import java.util.Arrays;

import static org.junit.Assert.*;

@RunWith(value = Parameterized.class)
public class CalculatorParameterizedTest {

    private static Calculator calculator;
    private int number;
    private int number2;
    private int result;

    @Parameterized.Parameters(name = "{index} : test Add ({0} + {1}) = {2}")
    public static Iterable<? extends Object> data() {
        return Arrays.asList(new Object[][] {
                {1,2,3},
                {2,3,5},
                {3,4,7}
        });
    };

    public CalculatorParameterizedTest(int number, int number2, int result) {
        this.number = number;
        this.number2 = number2;
        this.result = result;
    }

    @Before
    public void setUp() throws Exception {
        calculator = new Calculator();
    }

    @Test
    public void add() throws Exception {
        Assert.assertEquals(number,number2,result);
    }
//
//    @Test
//    public void subtract() throws Exception {
//    }
//
//    @Test
//    public void multiply() throws Exception {
//    }
//
//    @Test
//    public void divide() throws Exception {
//    }

}