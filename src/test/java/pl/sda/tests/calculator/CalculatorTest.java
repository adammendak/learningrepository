package pl.sda.tests.calculator;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class CalculatorTest {

    private Calculator calculator;

    @Before
    public void setup() {
        //Given
        calculator = new Calculator();
    }

    @Test
    public void addTest() {

        //When
        int result = calculator.add(1,2);

        //Then
        Assert.assertEquals(result,3);

    }

    @Test
    public void subtractTest() {

        //When
        int result = calculator.subtract(9,3);

        //Then
        Assert.assertEquals(result,6);
    }

    @Test(expected = ArithmeticException.class)
    public void divisionByZeroTest() {
        //when
        int result = calculator.divide(1,0);

    }

    @Test
    public void multiply() {

        //When
        int result = calculator.multiply(3,3);

        //Then
        Assert.assertEquals(result,9);
    }

    @Test
    public void divide() {

        //When
        int result = calculator.divide(9,3);

        //Then
        Assert.assertEquals(result,3);
    }
}
