package pl.sda.collections.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import pl.sda.collections.model.Author;

public class AuthorTest {
    private AuthorService authorService;

    @Before
    public void initAuthorService() {
        this.authorService = new AuthorService();
    }

    @Test
    public void ifAuthorIsProperlyAdded() {
        Author author = new Author("Juliusz", "Slowacki");
        authorService.addAuthor(author);
        authorService.addAuthor(author);

        Assert.assertTrue(this.authorService.findAllAuthors().size() == 1);
    }

    @Test
    public void removeAuthor() {
        Author author = new Author("Juliusz", "Slowacki");
        authorService.addAuthor(author);
        Assert.assertTrue(this.authorService.removeAuthor(author));

    }
}
