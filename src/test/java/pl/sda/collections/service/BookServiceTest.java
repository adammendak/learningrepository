/*
package pl.sda.collections.service;


import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import pl.sda.collections.model.Book;

import java.time.LocalDate;
import java.util.List;

public class BookServiceTest {

    private BookService bookService;

    @Before
    public void initBookService (){
        this.bookService = new BookService();
        Book book1 = new Book("Wladca Pierscieni", "Tolkien", LocalDate.of(2016,01,01));
        Book book2 = new Book("Wiedzmin", "Sapkowski", LocalDate.of(2016,01,01));
        this.bookService.addNewBook(book1);
        this.bookService.addNewBook(book2);
    }

    @Test
    public void verifyAddingBookToCollection() {
        Assert.assertNotNull("nie jest puste", bookService);

        List<Book> result = this.bookService.getAllBooks();
        Assert.assertEquals("Failed- book not added", 2, result.size());
    }

    @Test
    public void checkIfGoodBoogIsReturned() {
        Book returnedBook = this.bookService.findBookByTitleAndAuthor("Tolkien", "Wladca Pierscieni");
        Assert.assertNotNull(returnedBook);
        Assert.assertEquals("Zla ksiazka zostala zwrocona", "Tolkien", returnedBook.getAuthor());
        Assert.assertEquals("Zla ksiazka zwrocona", "Wladca Pierscieni", returnedBook.getTitle());
    }

    @Test
    public void checkIfBookIsRemoved() {

        Book book1 = new Book("Wladca Pierscieni", "Tolkien", LocalDate.of(2016,01,01));
        this.bookService.addNewBook(book1);
        Assert.assertTrue(this.bookService.removeBook(book1) == true);
        Assert.assertNotNull(this.bookService.getAllBooks());
    }

    @Test
    public void checkIfBookIsRemovedByAuthorName() {
        Book book1 = new Book("Wladca Pierscieni", "Tolkien", LocalDate.of(2016,01,01));
        this.bookService.removeBookByAuthor("Tolkien");
        Assert.assertNotNull("jest nullem", this.bookService.findBookByTitleAndAuthor("Tolkien","Wladca Pierscieni"));
    }


}*/
