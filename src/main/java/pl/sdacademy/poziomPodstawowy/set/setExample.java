package pl.sdacademy.poziomPodstawowy.set;

import pl.sdacademy.poziomPodstawowy.model.Person;
import pl.sdacademy.poziomPodstawowy.model.jobPosition;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

public class setExample {
    public static void main(String[] args) {
        Set<Person> people = new HashSet<>();
        Set<Person> treePeople = new TreeSet<>();

        Person person = createPerson("adam", "mendak", jobPosition.ARCHITECT,123412);
        Person person1 = createPerson("michal", "kepinski", jobPosition.MANAGER,12341234);
        Person person2 = createPerson("renata", "jachtoma", jobPosition.DEVELOPER,12341234);
        Person person4 = createPerson("arenata", "asdfajachtoma", jobPosition.DEVELOPER,123412);
        Person person3 = createPerson("dfsadrenata", "jachtweroma", jobPosition.DEVELOPER,123423);

        people.add(person);
        people.add(person1);
        people.add(person2);
        people.add(person3);
        people.add(person4);
        treePeople.add(person);
        treePeople.add(person1);
        treePeople.add(person2);
        treePeople.add(person3);
        treePeople.add(person4);

        for(Person Hashperson: people) {
            System.out.println(Hashperson);
        }
        System.out.println("-----------------------------");
        for(Person Treeperson: treePeople) {
            System.out.println(Treeperson);
        }


    }

    private static Person createPerson(String name, String surname, jobPosition position, Integer pesel) {
        Person person = new Person(name,surname, position, pesel);
        return person;
    }

}
