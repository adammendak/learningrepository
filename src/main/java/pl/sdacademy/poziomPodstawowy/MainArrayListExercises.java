package pl.sdacademy.poziomPodstawowy;

import pl.sdacademy.poziomPodstawowy.model.Book;

import java.util.ArrayList;
import java.util.List;

public class MainArrayListExercises {
    public static void main(String[] args) {
//
//        Book PanTadeusz = new Book ("Pan Tadeusz", "gruba ksiazka", 300, "Mickiewicz");
//        Book HarryPotter = new Book("Harry Potter", "satanistyczna ksiazka", 500,"J.K. Rowling");
//        Book Infero = new Book("Inferno", "Fajna powieść", 400, "ktośtam");
//
//        List<Book> books = new ArrayList<>();
//        books.add(PanTadeusz);
//        books.add(HarryPotter);
//        books.add(Infero);
//
//        books.stream()
//                .forEach(System.out::println);

        List<Integer> integers = new ArrayList<>();
        int sum = 0;
        int min =1;
        int max =1;

        for(int i=1; i<= 10; i++) {

             if(i % 3 == 0) {
                integers.add(i + (i-1));
            } else if (i %2 ==0){
                integers.add(i/2);
            } else {
                integers.add(i);
            };
            sum += integers.get(i-1);


        }

        for(int i = 0; i<integers.size();i++) {
            if(integers.get(i) < min) {
                min = integers.get(i -1);
            }
            if(integers.get(i) > max) {
                max = integers.get(i -1);
            }
        }

        System.out.println("suma to:" + sum);
        System.out.println("min to " + min);
        System.out.println("max to " + max);

        integers.stream()
                .forEach(System.out::println);




    }
}
