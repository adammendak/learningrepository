package pl.sdacademy.poziomPodstawowy;

public class MainArray {
    public static void main(String[] args) {
        int[] array = new int[10];

        for(int i = 0; i<array.length;i++) {
            array[i] = i + 1;
            System.out.println(array[i]);
        }

        System.out.println("-----------------------------");

        for(int i = 0; i<array.length;i++) {
            if(i % 3 == 0 && i != 0) {
                array[i]+= array[i-1];
                System.out.println(array[i]);
            }
        }

        System.out.println("-----------------------------");

        for(int i = 0; i<array.length;i++) {
            if(i % 2 == 0) {
                array[i] = array[i] /2;
                System.out.println(array[i]);
            }
        }

        System.out.println("-----------------------------");

        int sum = 0;

        for(int i =0;i<array.length;i++) {
            sum += array[i];
        }

        System.out.println("suma to : " + sum);

    }

}
