package pl.sdacademy.poziomPodstawowy.model;

public class Street implements Comparable<Street> {
    private int streetNumber;
    private int flatNumber;

    public Street(int streetNumber, int flatNumber) {
        this.streetNumber = streetNumber;
        this.flatNumber = flatNumber;
    }

    public int getStreetNumber() {
        return streetNumber;
    }

    public void setStreetNumber(int streetNumber) {
        this.streetNumber = streetNumber;
    }

    public int getFlatNumber() {
        return flatNumber;
    }

    public void setFlatNumber(int flatNumber) {
        this.flatNumber = flatNumber;
    }

    @Override
    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (o == null || getClass() != o.getClass()) return false;

        Street street = (Street) o;

        if(this.getStreetNumber() == street.getStreetNumber() && this.getFlatNumber() == street.getFlatNumber()) {
            return true;
        }

        return false;

//        if (streetNumber != street.streetNumber) return false;
//        return flatNumber == street.flatNumber;
    }

    @Override
    public int hashCode() {
//        int result = streetNumber;
        int result = 31 * (streetNumber + flatNumber);
        return result;
    }

    @Override
    public String toString() {
        return "Street{" +
                "streetNumber=" + streetNumber +
                ", flatNumber=" + flatNumber +
                '}';
    }

    @Override
    public int compareTo(Street o) {
        if(this.getStreetNumber() < o.getStreetNumber()){
            return -1;
        } else if (this.getStreetNumber() > o.getStreetNumber()){
            return 1;
        } else {
            if(this.getFlatNumber() < o.getFlatNumber()){
                return -1;
            } else if (this.getFlatNumber() > o.getFlatNumber()) {
                return 1;
            } else {
                return 0;
            }
        }
    }
}
