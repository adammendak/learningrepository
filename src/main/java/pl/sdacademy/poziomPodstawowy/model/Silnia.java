package pl.sdacademy.poziomPodstawowy.model;

public class Silnia {
    public static void main(String[] args) {
        int silnia = 5;

        System.out.println(silnia(silnia));

    }

    public static int silnia(int number) {
        if(number <2) {
            return 1;
        }
        return silnia(number -1) * number;

    }
}
