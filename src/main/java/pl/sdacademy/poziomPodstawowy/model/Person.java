package pl.sdacademy.poziomPodstawowy.model;

import javax.jnlp.PersistenceService;
import javax.print.attribute.standard.JobPriority;
import java.io.Serializable;

public class Person implements Comparable<Person> {
    private String name;
    private String surname;
    private jobPosition position;
    private Integer pesel;

    public Person(String name, String surname, jobPosition position, Integer pesel) {
        this.name = name;
        this.surname = surname;
        this.position = position;
        this.pesel = pesel;
    }

    public jobPosition getPosition() {
        return position;
    }

    public void setPosition(jobPosition position) {
        this.position = position;
    }

    public Integer getPesel() {
        return pesel;
    }

    public void setPesel(Integer pesel) {
        this.pesel = pesel;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Person person = (Person) o;

        if (name != null ? !name.equals(person.name) : person.name != null) return false;
        return surname != null ? surname.equals(person.surname) : person.surname == null;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (surname != null ? surname.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", position=" + position +
                '}';
    }

    @Override
    public int compareTo(Person o) {
        if(this.position.getPriority() < o.position.getPriority()){
            return -1;
        } else if (this.position.getPriority() > o.position.getPriority()) {
            return 1;
        } else {
            return 0;
        }
    }

    public int personOlderThan25() {
        String peselToCheck = String.valueOf(this.getPesel());

        if(peselToCheck.charAt(2) == 2)  {
            int age = (int)peselToCheck.charAt(0) *10 + (int)peselToCheck.charAt(1);
            if(age >25){
                return age;
            } else {
                return age;
            }
        } else {
            int age = 100 - (int)peselToCheck.charAt(0) * 10 - (int)peselToCheck.charAt(1);
            return age;
        }
    }
}
