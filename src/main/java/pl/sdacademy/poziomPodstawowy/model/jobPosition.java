package pl.sdacademy.poziomPodstawowy.model;

public enum jobPosition {
    MANAGER(10), ANALYST(8), ARCHITECT(6), SENIOR_DEVELOPER(4), DEVELOPER(2), JUNIOR(1);

    private jobPosition(int priority) {
        this.priority = priority;
    }

    private int priority;

    public int getPriority() {
        return priority;
    }
}
