package pl.sdacademy.poziomPodstawowy.model;

@FunctionalInterface
public interface Calculator {
    int compute (int a, int b);
}
