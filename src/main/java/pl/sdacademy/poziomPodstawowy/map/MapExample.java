package pl.sdacademy.poziomPodstawowy.map;

import pl.sdacademy.poziomPodstawowy.model.Person;
import pl.sdacademy.poziomPodstawowy.model.Street;
import pl.sdacademy.poziomPodstawowy.model.jobPosition;

import java.util.HashMap;
import java.util.Map;

public class MapExample {
    public static void main(String[] args) {
        Person person = createPerson("adam", "mendak", jobPosition.ARCHITECT,2627047);
        Person person1 = createPerson("michal", "kepinski", jobPosition.MANAGER, 26221231);
        Person person2 = createPerson("renata", "jachtoma", jobPosition.DEVELOPER, 70121212);
        Person person3 = createPerson("jacek", "iksinski", jobPosition.DEVELOPER, 801212122);
        Person person4 = createPerson("placek", "iksinski", jobPosition.DEVELOPER, 90121212);

        Map<String, Person> map = new HashMap<>();
        map.put(person.getName(), person);
        map.put(person1.getName(), person1);
        map.put(person2.getName(), person2);

        for(Map.Entry<String, Person> entry: map.entrySet()) {
            System.out.println(String.format("Klucz %s, wartosc %s", entry.getKey(),
                    entry.getValue().toString()));
        };

        System.out.println("-----------------");

        for (String key: map.keySet()) {
            System.out.println(String.format("klucz %s, wartosc %s", key, map.get(key).toString()
            ));
        }

        System.out.println("------------");

        Map<Integer, Person> peselMap = new HashMap<>();
        peselMap.put(person.getPesel(), person);
        peselMap.put(person1.getPesel(), person1);
        peselMap.put(person2.getPesel(), person2);
        peselMap.put(person3.getPesel(), person3);
        peselMap.put(person4.getPesel(), person4);

        for (Map.Entry<Integer, Person> peselPerson: peselMap.entrySet()) {
            System.out.println(String.format("klucz %d, wartosc %s", peselPerson.getKey(),
                  peselPerson.getValue().toString()  ));
        }
        System.out.println("-------------");
        System.out.println("osoby starsze niz 25 lat");
        //wyswietlamy wszystki osoby starsze niz 25 lat

//        for(Integer pesel: peselMap.keySet()) {
//            String yearFromPesel = pesel.toString().substring(0,2);
//            int year = Integer.parseInt(yearFromPesel);
//            if(yearFromPesel.charAt(2) == '2' || yearFromPesel.charAt(2) == '3'){
//                year += 2000;
//            } else {
//                year += 1900;
//            }
//            if(2017 - year > 25) {
//               Person p = map.get(pesel);
//                System.out.println(p.toString());
//            }
//        }


    }

    private static Person createPerson(String name, String surname, jobPosition position, Integer pesel) {
        Person person = new Person(name,surname, position, pesel);
        return person;
    }
}
