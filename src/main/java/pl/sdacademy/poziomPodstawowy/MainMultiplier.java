package pl.sdacademy.poziomPodstawowy;

import pl.sdacademy.poziomPodstawowy.model.Calculator;

import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.function.Function;

public class MainMultiplier {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Integer numberToMultiply;
        Integer howManyTimesMultiply;

            try {
                System.out.println("podaj liczbe: ");
                numberToMultiply = scanner.nextInt();

                System.out.println("podaj mnoznik: ");
                howManyTimesMultiply = scanner.nextInt();

                Calculator multiply = (a, b) -> a * b;

                System.out.println("wynik to : " + multiply.compute(numberToMultiply,howManyTimesMultiply));

            } catch (NumberFormatException |InputMismatchException e) {
                System.err.println("to nie jest liczba");

            }
        }
}