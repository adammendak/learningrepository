package pl.sdacademy.poziomPodstawowy.stream;

import pl.sdacademy.poziomPodstawowy.model.PersonStream;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class StreamExamples {

    public static void main(String[] args) {
        List<PersonStream> list = new ArrayList<>();
        list.add(new PersonStream("Adam","Mendak",31));
        list.add(new PersonStream("Maciej","kMendfdak",20));
        list.add(new PersonStream("Julian","iksimnski",10));
        list.add(new PersonStream("Dominik","kowalak",40));
        list.add(new PersonStream("Jan","kowalak",40));
        list.add(new PersonStream("Jan","kowdfafdalak",41));

        Optional<PersonStream> jan = list.stream()
                .filter(e-> e.getName().equals("Maciej"))
                .findFirst();
        System.out.println(jan.get());

        List wszyscyJanowie = list.stream()
                .filter(e -> e.getName().equals("Jan"))
                .collect(Collectors.toList());

        wszyscyJanowie.stream().forEach(System.out::println);
    }

}
