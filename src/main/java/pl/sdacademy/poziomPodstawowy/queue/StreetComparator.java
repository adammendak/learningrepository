package pl.sdacademy.poziomPodstawowy.queue;

import pl.sdacademy.poziomPodstawowy.model.Street;

import java.util.*;

public class StreetComparator {
    public static void main(String[] args) {
        Queue<Street> city = new PriorityQueue<>();
        Random randomGenerator = new Random();

        for (int i = 0; i< 100; i++) {

            Street streetName = new Street(randomGenerator.nextInt(10), randomGenerator.nextInt(10));
            city.offer(streetName);
        }
//        city.stream()
//                .forEach(System.out::println);

        for(int i = 0; i<city.size(); i++) {
            System.out.println(city.poll());
        }
        System.out.println("----------------------------------");

        Set<Street> hashCity = new HashSet<>();


        for (int i = 0; i< 100; i++) {

            Street streetName = new Street(randomGenerator.nextInt(10), randomGenerator.nextInt(10));
            hashCity.add(streetName);
        }

        hashCity.stream()
                .sorted()
                .forEach(System.out::println);
    }
}
