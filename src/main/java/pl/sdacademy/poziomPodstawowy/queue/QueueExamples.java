package pl.sdacademy.poziomPodstawowy.queue;

import pl.sdacademy.poziomPodstawowy.model.Book;
import pl.sdacademy.poziomPodstawowy.model.Person;
import pl.sdacademy.poziomPodstawowy.model.jobPosition;

import java.util.PriorityQueue;
import java.util.Queue;

public class QueueExamples {
    public static void main(String[] args) {
//        Queue<Book> books = new PriorityQueue<>();
//        Book book1 = new Book("tytul", "fajna ksiazka",1234,"autor");
//        Book book2 = new Book("tytul1", "fajna ksi234azka",1234,"autor");
//        Book book3 = new Book("tytul2", "fajna ksia1gfdzka",1234,"autor");
//        books.offer(book1);
//        books.offer(book2);
//        books.offer(book3);
//        System.out.println(books);
        Queue<Person> people = new PriorityQueue<>();
        Person person = createPerson("adam", "mendak", jobPosition.ARCHITECT,123);
        Person person1 = createPerson("michal", "kepinski", jobPosition.MANAGER,1234);
        Person person2 = createPerson("renata", "jachtoma", jobPosition.DEVELOPER,1234);

        people.offer(person);
        people.offer(person1);
        people.offer(person2);
        System.out.println(people.poll());
        System.out.println(people.poll());
        System.out.println(people.poll());




    }

    private static Person createPerson(String name, String surname, jobPosition position, Integer pesel) {
        Person person = new Person(name,surname, position, pesel);
        return person;
    }

}
