package pl.sdacademy.dto.cwiczenie1;

import java.io.*;
import java.nio.Buffer;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        List<Person> personList = getPersons();

        FileWriter writer = null;

        try {
            writer = new FileWriter("/home/adammendak/Workspace/WprowadzenieCzescDruga/personList.txt");

            for (int i = 0; i < personList.size(); i++) {
                writer.write(personList.get(i).getAme() + "\t");
                writer.write(personList.get(i).getSurname() + "\t");
                writer.write(personList.get(i).getJob() + "\t");
                writer.write("\n");
            }

            writer.flush();
            writer.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            BufferedReader bfrRead = new BufferedReader(new FileReader("/home/adammendak/Workspace/WprowadzenieCzescDruga/personList.txt"));
            String line;
            List<Person> result = new LinkedList<>();

            while((line = bfrRead.readLine()) != null) {
                String[] fields = line.split("\t");
                Person p = new Person(fields[0], fields[1], fields[2]);
                result.add(p);
                System.out.print (p.toString() + "\n");
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static List<Person> getPersons() {
        Person person1 = new Person("adam", "mendak", "dupa");
        Person person2 = new Person("maciek", "kowalski", "dupa");
        Person person3 = new Person("michal", "michalski", "dupa");


        List<Person> personList = new ArrayList<Person>();
        personList.add(person1);
        personList.add(person2);
        personList.add(person3);

        return personList;
    }

}
