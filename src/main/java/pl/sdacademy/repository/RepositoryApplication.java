package com.example.repository;

import com.example.repository.Model.Product;
import com.example.repository.repository.ProductRepository;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.logging.Logger;

@SpringBootApplication
public class RepositoryApplication implements CommandLineRunner {

	private ProductRepository productRepository;

	private org.slf4j.Logger LOG = LoggerFactory.getLogger(RepositoryApplication.class);

	@Autowired
	private RestTemplate restTemplate;

	@Bean
	public RestTemplate RestTemplate() {
		return restTemplate;

	}

	@Autowired
	public void productRepository(ProductRepository productRepository) {
		this.productRepository = productRepository;
	}

	public static void main(String[] args) {
		SpringApplication.run(RepositoryApplication.class, args);
	}

	@Override
	public void run(String... strings) throws Exception {

		Product productFromRestTemplate = restTemplate.getForObject("http://localhost:8080/api/products/1ab70fb0-b3d0-4070-9149-2395202b735c", Product.class);

		LOG.info("product got from rest template" + productFromRestTemplate.toString());


//		Product product1 = new Product();
//		product1.setName("testProduct");
//		product1.setDescription("test");
//		product1.setCategory("test");
//		product1.setType("GENERAL");
//		product1.setPrice(0.0);
//
//		productRepository.save(product1);
//
//		Product product2 = new Product();
//		product2.setName("anotherTest");
//		product2.setDescription("lolerz");
//		product2.setCategory("test");
//		product2.setType("GENERAL2");
//		product2.setPrice(1.1);
//
//		productRepository.save(product2);
//
//		List<Product> products = productRepository.findAll();
//
//		for (Product product: products) {
//			System.out.println(product.toString());
//		}
//
//		Product resultProduct = productRepository.findByType("GENERAL");
//
//		Product productToUpdate	= productRepository.findByType("GENERAL");
//
//		if(productToUpdate != null) {
//			productToUpdate.setPrice(4.4);
//			productToUpdate.setDescription("asdfasdfasf");
//			productRepository.save(productToUpdate);
//		}
//
//		productRepository.delete(productRepository.findByType("GENERAL"));
	}
}


