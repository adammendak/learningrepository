import java.util.Optional;

/**
 * Created by maniek on 20.06.17.
 */

class Computer {
    private SoundCard soundCard;

    public SoundCard getSoundCard() {
        return soundCard;
    }

    Computer(SoundCard soundCard) {
        this.soundCard = soundCard;
    }
}

class SoundCard {
    private Usb usb;

    public Usb getUsb() {
        return usb;
    }

    SoundCard(Usb usb) {
        this.usb = usb;
    }
}

class Usb {
    String getVersion() {
        return "3.1";
    }
}

public class JavaOptionalTest {
    public static void main(String[] args) {
        Computer nonExistingComputer = null;
        Computer existingComputer = new Computer(new SoundCard(new Usb()));
        Computer computerWithoutSoundCard = new Computer(null);
        Computer computerWithSoundCardWithoutUsb = new Computer(new SoundCard(null));

        String myUsbVersion =
                null != getUsbVersion(existingComputer) ? getUsbVersion(existingComputer) : "UNKNOWN";
        System.out.println("usb version of existing computer: " + myUsbVersion);

        String usbVersionOfNonExistingComp =
                null != getUsbVersion(nonExistingComputer) ? getUsbVersion(nonExistingComputer) : "UNKNOWN";
        System.out.println("usb version of non existing computer: " + usbVersionOfNonExistingComp);

        String usbVersionOfRasberryPi =
                null != getUsbVersion(computerWithoutSoundCard) ? getUsbVersion(computerWithoutSoundCard) : "UNKNOWN";
        System.out.println("usb version of rasberry pi: " + usbVersionOfRasberryPi);

        String usbVersionFromSoundCardWithoutUsb =
                null != getUsbVersion(computerWithSoundCardWithoutUsb) ? getUsbVersion(computerWithSoundCardWithoutUsb) : "UNKNOWN";
        System.out.println("usb version of computer with soundcard without usb: " + usbVersionFromSoundCardWithoutUsb);
    }

    static String getUsbVersion(Computer computer) {
        String result = null;

        if (null != computer) {
            SoundCard soundCard = computer.getSoundCard();
            if (null != soundCard) {
                Usb usb = soundCard.getUsb();
                if (null != usb) {
                    result = usb.getVersion();
                }
            }
        }
        return result;
    }

    static String getUsbVersionWithOptional(Computer computer) {
        return Optional.ofNullable(computer)
                .map(Computer::getSoundCard)
                .map(SoundCard::getUsb)
                .map(Usb::getVersion)
                .orElse("Unknown");
    }

}
