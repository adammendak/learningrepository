import java.util.function.Function;

/**
 * Created by maniek on 03.07.17.
 */
public class OptionalWithFunction {
    public static void main(String[] args) {
        Function<String, Integer> mapper = new Function<String, Integer>() {
            @Override
            public Integer apply(String s) {
                return 5;
            }
        };
    }
}

class MyFunction implements Function<String, Integer> {

    @Override
    public Integer apply(String s) {
        return null;
    }
}

