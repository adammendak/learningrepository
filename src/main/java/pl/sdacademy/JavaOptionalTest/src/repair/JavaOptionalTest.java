package repair;

import java.util.Optional;

/**
 * Created by maniek on 20.06.17.
 */

class Computer {
    private Optional<SoundCard> soundCard;

    public Optional<SoundCard> getSoundCard() {
        return soundCard;
    }

    Computer(SoundCard soundCard) {
        this.soundCard = Optional.ofNullable(soundCard);
    }
}

class SoundCard {
    private Optional<Usb> usb;

    public Optional<Usb> getUsb() {
        return usb;
    }

    SoundCard(Usb usb) {
        this.usb = Optional.ofNullable(usb);
    }
}

class Usb {
    String getVersion() {
        return "3.1";
    }
}

public class JavaOptionalTest {
    public static void main(String[] args) {
        Optional<Computer> nonExistingComputer = Optional.empty();
        Optional<Computer> existingComputer = Optional.of(new Computer(new SoundCard(new Usb())));
        Optional<Computer> computerWithoutSoundCard = Optional.of(new Computer(null));
        Optional<Computer> computerWithSoundCardWithoutUsb = Optional.of(new Computer(new SoundCard(null)));

        System.out.println(extractVersion(nonExistingComputer));
        System.out.println(extractVersion(existingComputer));
        System.out.println(extractVersion(computerWithoutSoundCard));
        System.out.println(extractVersion(computerWithSoundCardWithoutUsb));
    }

    public static String extractVersion(Optional<Computer> computer) {
//        computer.flatMap(Computer::getSoundCard);
        String result =
        computer.flatMap(Computer::getSoundCard)
                .flatMap(SoundCard::getUsb)
                .map(Usb::getVersion)
                .orElse("Unknown");

        return result;
    }
}
