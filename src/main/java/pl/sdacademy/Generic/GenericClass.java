package pl.sdacademy.Generic;

import javax.naming.Name;
import java.util.List;

public class  GenericClass<T extends Nameable> {

    private List<T> object;

    public List<T> getObject() {
        return object;
    }

    public GenericClass(List<T> object) {

        this.object = object;
    }

    public String getConcatenatedNames() {
        StringBuilder sb = new StringBuilder();
        for (T t : object) {
            sb.append(t.getName());
        }
        return sb.toString();
    }
}
