package pl.mije.sda.java.metadane;

import java.io.*;

class Bazowa {

    Integer poleBazowe = 20;

    Bazowa() {
        System.out.println("Konstruktor bazowy");
    }

}


public class Serializacja extends Bazowa implements Serializable{

    transient Integer poleInt = 10;

    public static void main(String[] args) throws Exception{

        Serializacja s = new Serializacja();
        s.poleBazowe = 30;
        System.out.println(s);

        zapisz(s);

        s = getSerializacja();
        System.out.println(s);


    }

    public static Serializacja getSerializacja() throws IOException, ClassNotFoundException {
        Serializacja s;FileInputStream fis = new FileInputStream("plik.ser");
        ObjectInputStream ois = new ObjectInputStream(fis);
        Object object = ois.readObject();
        ois.close();
        fis.close();
        s = (Serializacja) object;
        return s;
    }

    public static void zapisz(Serializacja s) throws IOException {
        FileOutputStream fileOut = new FileOutputStream("plik.ser");
        ObjectOutputStream out = new ObjectOutputStream(fileOut);
        out.writeObject(s);
        out.close();
        fileOut.close();
    }

    @Override
    public String toString() {
        return "Serializacja{" +
                "poleBazowe=" + poleBazowe +
                ", poleInt=" + poleInt +
                '}';
    }
}