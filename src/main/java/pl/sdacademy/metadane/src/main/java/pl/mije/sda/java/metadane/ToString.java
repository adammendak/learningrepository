package pl.mije.sda.java.metadane;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

public class ToString {

    public static void main(String[] args) throws ClassNotFoundException, IllegalAccessException, InstantiationException, NoSuchMethodException, InvocationTargetException {

        Osoba osoba = new Osoba();
        osoba.setImie("Adam");
        osoba.setNazwisko("Mendak");
        osoba.setWiek(31);

        System.out.println(ToString.toStringJsonFromCzytamPola(CzytamPola.czytaj(osoba)));

        System.out.println(ToString.toStringJson(osoba));

        System.out.println(ToString.toString(osoba));

        String wynik = ToString.toString(osoba);

        osoba = (Osoba) ToString.fromString(wynik);

        System.out.println(osoba);

//
//        Class<?> clazz = Class.forName("pl.mije.sda.java.metadane.ToString");
////        Object instance = (ToString)clazz.newInstance();
//        Constructor<?> constructor = clazz.getConstructor(String.class);
//        Object instance = constructor.newInstance("Adam", "Mendakw",21);
//        System.out.println(instance);

    }

    //tak nie robic tylko korzystac z mechanizmu refleksji i odwolywac sie przez field.getName() i field.get(object)
    public static String toStringJsonFromCzytamPola(String jsonify) {
        String[] split = jsonify.split("(,|:)");
        String result = "{";
        for(int i=0; i<split.length;i++) {

            if(i%2 == 0) {
                result += "\"";
                result += split[i];
                result += "\":";
            } else if(i%2 !=0 && i != split.length-1) {
                result += "\"";
                result += split[i];
                result += "\",";
            }
            else {
                result += "\"";
                result += split[i];
                result += "\"";
            }

        }
        result += "}";

        return result;

    }

    public static String toStringJson(Object object) {
        Field[] fields  = object.getClass().getDeclaredFields();

        String result = "";
        for (Field field : fields) {
            result += field.getName();
            field.setAccessible(true);
            try {
                result = result + ":" + field.get(object) + ",";
            } catch (IllegalArgumentException e) {
            } catch (IllegalAccessException e) {
            }
        }
        result = result.substring(0, result.length()-1);
        return ToString.toStringJsonFromCzytamPola(result);
    }

    public static String toString (Object object) throws IllegalAccessException{
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        for (Field f : object.getClass().getDeclaredFields()) {
            f.setAccessible(true);
            sb.append("\"");
            sb.append(f.getName());
            sb.append("\":\"");
            sb.append(f.get(object));
            sb.append("\",");
        }
        sb.deleteCharAt(sb.length()-1);
        sb.append("}");
        return sb.toString();
    }

    public static Object fromString(String object) {
        return null;
    }

    public static Object fromString(String zawartosc, Class klass) throws IllegalAccessException, InstantiationException {
        Map<String, String> mapa = stworzMape(zawartosc);
        Object instancja = klass.newInstance();
        for(Field f : klass.getDeclaredFields()) {
            f.setAccessible(true);
            if(mapa.containsKey(f.getName())) {
                f.set(instancja, mapa.get(f.getName()));
            }
        }

        return instancja;


    }

    public static Map<String, String> stworzMape(String zawartosc) {
        zawartosc = zawartosc.replace("}", "");
        zawartosc = zawartosc.replace("{", "");
        String[] nazwaWartosc = zawartosc.split(",");
        Map<String, String> mapa = new HashMap<>();
        for(String s : nazwaWartosc) {
            String[] poDwukropku = s.split(":");
            mapa.put(usunZnaki(poDwukropku[0]), usunZnaki(poDwukropku[1]));
        }
        return mapa;
    }

    public static String usunZnaki(String s) {
        if(s.startsWith("\"")){
            s = s.substring(1,s.length());
        }
        if(s.endsWith("\"")) {
            s = s.substring(0,s.length()-1);
        }
        return s;
    }

}
