package metadane;

import org.junit.Assert;
import org.junit.Test;

import pl.mije.sda.java.metadane.CzytamPola;

public class MetadataTest {

	@Test
	public void testCzytajPola(){
		CzytamPola czytamPola = new CzytamPola();
		String wynikA = czytamPola.czytaj(new ATest());
		String wynikB = czytamPola.czytaj(new BTest());
		String wynikC = czytamPola.czytaj(new CTest());
		Assert.assertEquals("poleA:A", wynikA);
		Assert.assertEquals("poleB:b", wynikB);
		Assert.assertEquals("poleC:c,poleD:d", wynikC);
		Class aTest = ATest.class;
		aTest = czytamPola.getClass();
	}
}

class ATest{
	String poleA = "A";
}

class BTest{
	String poleB = "b";
}

class CTest{
	String poleC = "c";
	String poleD = "d";
}