package pl.sdacademy.BuilderPattern;


public class MainBuilder {
    public static void main(String[] args) {
        UserWithBuilder.UserBuilder builder = new UserWithBuilder.UserBuilder("John", "Doe");
        builder.address("30")
                .phone("12313123123")
                .age(31)
                .build();
        System.out.println(builder.build());

        Car.CarBuilder builderCar = new Car.CarBuilder("something");
        builderCar.Name("lol").Price(12).build();
        System.out.println(builderCar.build());

    }
}
