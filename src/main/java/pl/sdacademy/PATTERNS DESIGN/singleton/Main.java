package pl.sdacademy.singleton;

import pl.sdacademy.model.Person;

public class Main {
    public static void main(String[] args) {
    	System.out.println("Inside main()"); 
//        SingletonByBillPugh.print();
//        SingletonByBillPugh.getInstance();
        EagerCreatedSingleton singleton = EagerCreatedSingleton.getInstance();
        System.out.println("stworzona pierwsza instancja singletona: " + singleton.toString());
        EagerCreatedSingleton singleton2 = EagerCreatedSingleton.getInstance();
        System.out.println("stworzona druga instancja singletona: " +singleton2.toString());
        System.out.println("czy sa rowne? ");
        System.out.println(singleton.equals(singleton2));

        System.out.println(Main.class.getSimpleName());

        System.out.println(Person.getInstance().toString());

//        SingletonByBillPugh.getInstance();
        SingletonByBillPugh.print();
    }
}
