package com.sdacademy.design_patterns.functional;

import com.sdacademy.design_patterns.model.Country;
import com.sdacademy.design_patterns.model.CountryFactory;

import java.util.Optional;

public class Optionals {
    Optional<Country> country = Optional.empty();

    void test() {
        Country poland = CountryFactory.getCountry(CountryFactory.polandCode);
        country = Optional.ofNullable(poland);
    }
}
