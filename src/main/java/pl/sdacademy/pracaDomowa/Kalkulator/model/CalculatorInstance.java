package src.main.java.pl.sdacademy.pracaDomowa.Kalkulator.model;

import java.util.List;

public class CalculatorInstance implements Calculator {
    private List<Integer> history;

    public CalculatorInstance(List<Integer> history) {
        this.history = history;
    }

    Calculator adder = (a,b) -> a + b;
    Calculator substractor = (a,b) -> a - b;
    Calculator multiplier = (a,b) -> a * b;
    Calculator divider = (a,b) -> a / b;

}
