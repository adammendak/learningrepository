package src.main.java.pl.sdacademy.pracaDomowa.Kalkulator.model;

@FunctionalInterface
public interface Calculator {
    int compute (int a, int b);
}
