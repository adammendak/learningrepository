package pl.sdacademy.thread;

import pl.sdacademy.singleton.Main;

public class ThreadTest {
    public static void main(String[] args) {
        Runnable job1 = () -> {
            System.out.println("a");
            System.out.println("b");
            System.out.println("c");
            System.out.println(Thread.currentThread().getName());
        };

        Runnable job2 = new Job2();

        Thread worker1 = new Thread(job1);
        Thread worker2 = new Thread(job2);
        try {
            worker1.start();
            worker1.join();
            worker2.start();
            worker2.join();
            System.out.println("finish");
        } catch (InterruptedException e ) {
            e.printStackTrace();
        }
    }
}
// te dwa joby tak samo zadzialaja, widac roznice w lambdach jak szybko sie implementuje
class Job2 implements Runnable {
    @Override
    public void run() {
        System.out.println("d");
        System.out.println("e");
        System.out.println("f");
        System.out.println(Thread.currentThread().getName());
    }
}