package pl.sdacademy.singleton;

public class Sandbox {
    private static Sandbox INSTANCE;

    private String text;

    public Sandbox() {
        this.text = "dupa";
    }

    public static synchronized Sandbox getInstance() {
        if( INSTANCE == null) {
            INSTANCE = new Sandbox();
        }
        return INSTANCE;
    }


}
