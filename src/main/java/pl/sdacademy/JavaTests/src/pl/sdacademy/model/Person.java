package pl.sdacademy.model;

public class Person {

	private static Person PERSON;

	private String name;
	private String surname;

	private Person(String name, String surname) {
		this.name = name;
		this.surname = surname;
	}

	public static Person getInstance() {
		if(null == PERSON) {
			synchronized (Person.class) {
				if(null == PERSON) {
					PERSON = new Person("Adam ", "Mendak");
				}
			}
		}
		return PERSON;
	}

	@Override
	public String toString() {
		return "Person{" +
				"name='" + name + '\'' +
				", surname='" + surname + '\'' +
				'}';
	}

	public static void describe() {
		System.out.println("Person class");
	}
	
	static {
		System.out.println("Loading class in progress...");
	}

	public String getName() {
		return name;
	}

	public String getSurname() {
		return surname;
	}
}
