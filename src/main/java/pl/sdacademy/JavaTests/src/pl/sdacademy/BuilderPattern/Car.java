package pl.sdacademy.BuilderPattern;

public class Car {
    private String type; //required
    private String name; //optional
    private double price; //optional

    private Car(CarBuilder builder) {
        this.type = builder.type;
        this.name = builder.name;
        this.price = builder.price;
    }

    public String getType() {
        return type;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return "Car{" +
                "type='" + type + '\'' +
                ", name='" + name + '\'' +
                ", price=" + price +
                '}';
    }

    public static class CarBuilder {
        private String type;
        private String name;
        private double price;

        public CarBuilder(String type) {
            this.type = type;
        }

        public CarBuilder Name(String name) {
            this.name = name;
            return this;
        }

        public CarBuilder Price(double price) {
            this.price = price;
            return this;
        }

        public Car build() {
            return new Car (this);
        }
    }


}
