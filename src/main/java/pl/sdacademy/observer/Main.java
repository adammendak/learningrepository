package pl.sdacademy;

public class Main {

	public static void main(String[] args) {
//		Observer DianaSandra = new MyObserver("DianaSandra");
//		Observer DianaDiana = new MyObserver("DianaDiana");
//
//		RealObservable observable = new RealObservable();
//		observable.registerObserver(DianaDiana);
//		observable.registerObserver(DianaDiana);
//
//		observable.addNewNewsletter("one");
//		observable.addNewNewsletter("two");

        MyObserver observer1 = new MyObserver("Adam");
        MyObserver observer2 = new MyObserver("Maciek");

        RealObservable facebook = new RealObservable();
        facebook.registerObserver(observer1);
        facebook.registerObserver(observer2);

        facebook.addNewNewsletter("lol1");
        facebook.addNewNewsletter("lol2");
        facebook.notifyObservers("lol");





	}

}
