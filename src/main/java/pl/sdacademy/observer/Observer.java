package pl.sdacademy;

public interface Observer {
	void update(String info);
}
