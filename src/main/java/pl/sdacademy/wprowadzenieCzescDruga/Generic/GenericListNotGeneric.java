package pl.sdacademy.Generic;

import java.util.LinkedList;
import java.util.List;

public class GenericListNotGeneric {
    public static void main(String[] args) {
        //przyklad ze sie dodalo ale wywalilo na koncu
        List<String> list = new LinkedList<>();
        list.add("a");
        list.add("b");
        list.add("c");
        list.add("d");
        addIntegerToList(list);

        for (String string: list) {
            System.out.println(string);

        }
    }

    public static void addIntegerToList(List list) {
        list.add(new Integer(2));
    }
}
