package pl.sdacademy.exceptions.excercises;

import pl.sdacademy.exceptions.excercises.model.NumberChecker;

public class Main {
    public static void main(String[] args) {

        NumberChecker numberChecker = new NumberChecker();
        numberChecker.isNumber("1234");
        numberChecker.isNumber("lol");
        numberChecker.isNumber("1234");
        numberChecker.isNumber("dupa");

        Integer i = 256;
        System.out.println(Integer.toString(i, 36));

//      Integer x = new Integer("123");
//      Integer x2 = new Integer("123");
        Integer x = Integer.valueOf("123");
        Integer x2 = Integer.valueOf("123");

        if(x.equals(x2)){
            System.out.println("git");
        }

        if(x == x2){
            System.out.println("git tez ");
        } else{
            System.out.println("nie git");
        }

        String s = "aaa";
        String s2 = "aaa";

        if(s == s2){
            System.out.println("git tez ");
        } else{
            System.out.println("nie git");
        }


    }
}
