package pl.sdacademy.dto;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

public class SerializableObject implements Serializable {

    private static final long serialVersionUID = 1;
    private String id;
    private Integer i1;
    private Long long1;
    private List<String> someList;
    private String[] array;

    public String getId() {
        return id;
    }

    @Override
    public String toString() {
        return "SerializableObject{" +
                "id='" + id + '\'' +
                ", i1=" + i1 +
                ", long1=" + long1 +
                ", someList=" + someList +
                ", array=" + Arrays.toString(array) +
                '}';
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getI1() {
        return i1;
    }

    public void setI1(Integer i1) {
        this.i1 = i1;
    }

    public long getLong1() {
        return long1;
    }

    public void setLong1(long long1) {
        this.long1 = long1;
    }

    public List<String> getSomeList() {
        return someList;
    }

    public void setSomeList(List<String> someList) {
        this.someList = someList;
    }

    public String[] getArray() {
        return array;
    }

    public void setArray(String[] array) {
        this.array = array;
    }
}