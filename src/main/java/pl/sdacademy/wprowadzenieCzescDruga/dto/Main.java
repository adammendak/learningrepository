package pl.sdacademy.dto;

import java.io.*;
import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {

        int hex = 0x1FEA;
        int bin = 0b10101010;

        try(ObjectOutputStream objecOutput = new ObjectOutputStream(new FileOutputStream("/home/adammendak/Workspace/WprowadzenieCzescDruga/iotest.txt"))){
            objecOutput.writeObject(getDefaultSerializableObject());
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static SerializableObject getDefaultSerializableObject() {

        SerializableObject obj = new SerializableObject();
        obj.setI1(666);
        obj.setLong1(1L);
        obj.setArray(new String[] {"aaa", "bbb", "ccc"});
        obj.setSomeList(new ArrayList<>());
        obj.getSomeList().add("lol1");
        obj.getSomeList().add("lol2");
        obj.getSomeList().add("lol3");
        obj.getSomeList().add("lol4");

        obj.toString();
        return obj;
    }

    public static void readAndPrintObject() {
        try(ObjectInputStream objInput = new ObjectInputStream(new FileInputStream("/home/adammendak/Workspace/WprowadzenieCzescDruga/iotest.txt"))){
//          Object o = objInput.readObject();
//            System.out.println(o.getClass().getName());
//            System.out.println(o.toString());
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
}
