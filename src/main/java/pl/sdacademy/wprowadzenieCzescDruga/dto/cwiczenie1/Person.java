package pl.sdacademy.dto.cwiczenie1;

import pl.sdacademy.Generic.Nameable;

import java.util.List;

public class Person implements Nameable{
    public String name;
    public String surname;
    public String job;

    public Person(String name, String surname, String job) {
        this.name = name;
        this.surname = surname;
        this.job = job;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", job='" + job + '\'' +
                '}';
    }

    public String getName() {

        return name;
    }

    public void setName(String ame) {
        this.name = ame;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public <K> K genericMethog(List<K> object) {
//        object.size();
        return object.get(object.size() -1);
    }

}
