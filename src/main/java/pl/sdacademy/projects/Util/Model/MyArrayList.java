package pl.sdacademy.projects.Util.Model;

import java.util.Arrays;
import java.util.Iterator;
import java.util.stream.Stream;

public class MyArrayList<T extends Comparable<T>> extends MyAbstractDynamicArray<T>
        implements MyList<T>  {

    public MyArrayList() {
        super();
    }

    public MyArrayList(int initialSize) {
        super(initialSize);
    }

    @Override
    public void add(T t){

        ensureCapacity();

        array[index] = t;
        index++;
    }

    private void ensureRange(int index) {
        if(index < 0 || index >= this.index){
            throw new IndexOutOfBoundsException("Wyjscie poza zakres tablisy" + index);
        }
    }

    @Override
    public T get(int index) {
        ensureRange(index);
        return array[index];
    }

    @Override
    public void add(int index, T t) {
        //toDo
    }

    @Override
    public boolean remove(int i) {

        if(i < this.index && i >= 0){
            for(int j = i; j < this.index; j++ ){
                array[j] = array[j + 1];
            }
            index--;
            return true;
        } else {
            throw new IndexOutOfBoundsException("wyhscie poza zakres tablicy" + index);
        }

    }

    @Override
    public boolean remove(T t) {
        return false;
    }

    @Override
    public boolean contains(T t) {
        return stream().anyMatch(p -> p.equals(t));
    }

    @Override
    public int size(){
        return index;
    }

    @Override
    public boolean isEmpty(){
        return index == 0;
    }

    @Override
    public Stream<T> stream(){

        return Arrays.stream(array, 0, index);

    }

    @Override
    public Iterator<T> iterator() {
        return new Iterator<T>() {

            private int internalIndex = 0;

            @Override
            public boolean hasNext() {
                return internalIndex < index;
            }

            @Override
            public T next() {
                return array[internalIndex++];
            }
        };
    }

    public int binarySearch(T search) {
        int start = 0;
        int stop = index -1;
        int middle = index/2;

        while(start <= stop) {

            if(array[middle].compareTo(search) < 0) {
                start = middle + 1;
            } else if (array[middle].compareTo(search) == 0) {
                return middle;
            } else {
                stop = middle -1;
            }

            middle = (start + stop)/2;
        }
        return -1;
    }

}
