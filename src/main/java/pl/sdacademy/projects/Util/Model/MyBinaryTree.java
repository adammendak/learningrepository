package pl.sdacademy.projects.Util.Model;

public class MyBinaryTree<T extends Comparable<T>> {

    private MyBinaryTreeNode<T> root;

    public void add(T value) {

        MyBinaryTreeNode<T> newNode = new MyBinaryTreeNode<>(value);

        if (root == null) {
            root = newNode;
            return;
        }

        MyBinaryTreeNode<T> tmp = root;

        MyBinaryTreeNode<T> parent;

        while(true) {
            parent = tmp;

            if(tmp.getValue().equals(value)) {
                System.out.println("Duplikat");
                return;
            }

            if(value.compareTo(tmp.getValue()) < 0) {
                tmp = tmp.getLeftChild();
            }

            if(tmp == null) {
                parent.setLeftChild(newNode);
                return;
            } else {
                tmp = tmp.getRightChild();
                if (tmp == null) {
                    parent.setRightChild(newNode);
                    return;
                }
            }
        }

    }

    public void inOrderTraversal() {
        inOrderTraversal(root);
    }

    public boolean contains(T value) {
        MyBinaryTreeNode<T> tmp = root;
        while(tmp.getValue().equals(value) != true) {

            if(value.compareTo(tmp.getValue()) < 0) {
                tmp = tmp.getLeftChild();
            } else {
                tmp = tmp.getRightChild();
            }

            if(tmp == null) {
                return false;
            }
        }

        return true;
    }

    private void inOrderTraversal(MyBinaryTreeNode<T> node) {

        if(node != null) {
            inOrderTraversal(node.getLeftChild());

            System.out.println(node.getValue());

            inOrderTraversal(node.getRightChild());
        }

    }

    public int size() {
        return size(root,0);
    }

    private int size(MyBinaryTreeNode<T> node, int counter) {
        if(node != null) {
            counter ++;
            size(node.getLeftChild(), counter);
            size(node.getRightChild(), counter);
        }
        return counter;
    }


}
