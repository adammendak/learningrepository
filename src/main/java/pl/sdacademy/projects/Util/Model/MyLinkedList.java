package pl.sdacademy.projects.Util.Model;

import java.util.Iterator;
import java.util.stream.Stream;

public class MyLinkedList<T> implements Iterable<T> {

    private MyNode<T> head;

    private MyNode<T> tail;

    public void add(T t) {
        MyNode<T> node = new MyNode<>();
        node.setElement(t);

        //gdy nie ma pierwszego elementu
        if(head == null) {
            head = node;
            tail = node;
        } else {
            tail.setNext(node);
            tail = node;
        }
    }

    public void traverse() {
        MyNode<T> tmp = head;

        while(true){
            if(tmp==null){
                break;
            }
            System.out.println(tmp.getElement());
            tmp = tmp.getNext();
        }
    }

    @Override
    public Iterator<T> iterator() {
        return new Iterator<T>() {

            MyNode<T> internalNode = head;

            @Override
            public boolean hasNext() {
                return internalNode.getNext() != null;
            }

            @Override
            public T next() {
                T temp = internalNode.getElement();
                internalNode = internalNode.getNext();
                return temp;
            }
        };
    }

    public boolean addAfter(T after, T t) {

        MyNode<T> tmp = head;
        MyNode<T> refNode = new MyNode<>();


        while (true) {
            if (tmp == null) {
                return false;
            }

            if (tmp.equals(after)) {
                refNode = tmp;
                break;

            }

            tmp = tmp.getNext();
        }

        if (refNode != null) {
            //add element after the target node
            MyNode<T> nd = new MyNode<>();
            nd.setElement(t);
            nd.setNext(tmp.getNext());
            if (tmp == tail) {
                tail = nd;
            }
            tmp.setNext(nd);
        } else {

            System.out.println("unab;e tp find given element");
            return false;
        }
        return true;
    }

    public void removeAfter(T after) {
        MyNode<T> tmp = head;
        MyNode<T> refNode;

        while(true) {
            if(tmp == null) {
                return;
            }

            if(tmp.equals(after)) {
                refNode = tmp;
                break;
            }

            tmp = tmp.getNext();

            if(refNode == tail) {
                System.out.println("Znaleziono ogon - wyjście z metody");
                return;
            }

            if(refNode.getNext() == tail) {
                tail = refNode;
                refNode.setNext(null);
                System.out.println("refNode.getNext() == tail");
            } else {
                refNode = refNode.getNext().getNext();
                System.out.println("else");
            }

        }


    }

    public void deleteHead() {
        if(head == null) {
            System.out.println("head is null");
        }

        MyNode<T> tmp = head;
        head = tmp.getNext();
        if(head == null) {
            tail = null;
        }
    }

    public Stream<T> stream() {
        Stream.Builder<T> builder = Stream.builder();

        MyNode<T> tmp = head;

        while(true) {
            if(tmp == null) {
                break;

            }

            builder.add(tmp.getElement());

            tmp = tmp.getNext();
        }

        return builder.build();
    }
}
