package pl.sdacademy.projects.Util;

import pl.sdacademy.projects.Util.Model.MyArrayList;
import pl.sdacademy.projects.Util.Model.MyLinkedList;
import pl.sdacademy.projects.Util.Model.MyList;
import pl.sdacademy.projects.Util.Model.MyStack;

import java.util.Iterator;

public class Main {

    public static void main(String[] args) {

        MyList<String> list = new MyArrayList<>();

                list.add("ala");
        list.add(" ma");
        list.add(" kota");
        list.add(", a kot");
        list.add(" ma");
        list.add(" ale");

//        for(String str : list ) {
//            System.out.println(str);
//        }


//        list.stream()
//                .forEach(System.out::print);


        Iterator<String> iterator = list.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }

        System.out.println("------------------------------------");

        System.out.println(list.contains("ala"));
        System.out.println(list.contains("dupa"));

        System.out.println("------------------------------------");

        list.remove(2);
        list.remove(0);
        list.stream().forEach(System.out::println);

        System.out.println("------------------------------------");

        MyStack<Integer> stack = new MyStack<>();
        stack.push(10);
        stack.push(20);
        stack.push(30);
        stack.push(40);

        stack.traverse();

        System.out.println(stack.peek());

        stack.pop();
        stack.pop();
        stack.pop();

        System.out.println("-----------------------------");

        stack.traverse();

        System.out.println("-----------------------------");

        MyLinkedList<Integer> list2 = new MyLinkedList<>();
        list2.add(5);
        list2.add(52);
        list2.add(1);
        list2.add(2);

//        list2.traverse();

        for(Integer integer : list2) {
            System.out.println(integer);
        }

        System.out.println("======================================");

        list2.stream().filter(s -> s.intValue() >= 20 )
                .forEach(System.out::println);

    }
}
