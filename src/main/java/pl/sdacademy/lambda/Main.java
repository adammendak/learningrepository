package pl.sdacademy.lambda;

import pl.sdacademy.lambda.model.Adder;
import pl.sdacademy.lambda.model.Computable;
import pl.sdacademy.lambda.model.Mulitplier;

public class Main {
    public static void main (String[] args) {

        Computable adder = new Adder();
        Computable multiplier = new Mulitplier();

        Computable tab[] = new Computable[2];
        tab[0] = adder;
        tab[1] = multiplier;

        for (Computable element: tab) {
            System.out.println(element.toString());
            System.out.println(element.compute(4,5));

        }

//        System.out.println(adder.compute(12,23));
//        System.out.println(multiplier.compute(12,23));

    }
}
