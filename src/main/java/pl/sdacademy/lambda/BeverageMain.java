package pl.sdacademy.lambda;

import pl.sdacademy.lambda.model.Beverage;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class BeverageMain {
    public static void main(String[] args) {
        List<Beverage> beverages = new ArrayList<>();

        beverages.add(new Beverage("Black", 3.0, 0.2, true));
        beverages.add(new Beverage("Pepsi", 4.0, 1.5, true));
        beverages.add(new Beverage("Helena", 1.5, 1.5, true));
        beverages.add(new Beverage("Kwas Chlebowy", 4.0, 1.0, true));
        beverages.add(new Beverage("Wyborowa", 18.0, 0.5, false));
        beverages.add(new Beverage("Piwo", 2.2, 0.5, false));
        beverages.add(new Beverage("Piwo", 2.2, 0.5, false));

        beverages.stream()
        .filter(b -> b.getCapacity() < 1.5
                && b.isSoft() == true
                && b.getPrice() > 1.0)
//        .sorted((a,b) -> (int)(a.getPrice() - b.getPrice()))
                // * -1 powoduje ze odwraca kolejnosc
        .sorted((a,b) -> Double.valueOf(a.getPrice()).compareTo(b.getPrice()) * -1)
        .forEach(System.out::println);





    }
}
