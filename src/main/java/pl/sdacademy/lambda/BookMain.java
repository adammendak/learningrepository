package pl.sdacademy.lambda;

import com.sun.org.apache.xpath.internal.SourceTree;
import com.sun.xml.internal.ws.policy.privateutil.PolicyUtils;
import pl.sdacademy.lambda.model.Book;
import pl.sdacademy.lambda.model.BookComparator;
import pl.sdacademy.lambda.model.Library;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class BookMain {

    public static void main(String[] args) {

        List<Book> books = new ArrayList<>();

        books.add(new Book("pan tadeka", "Adam", "Mickiewicz", 500,2002,"12341234"));
        books.add(new Book("pan maciek", "Roman", "Michnik", 432,1986,"7243654"));
        books.add(new Book("pan slawek", "Slawek", "Malysz", 162,1943,"13245324"));
        books.add(new Book("pan lukasz", "Zenon", "Slowacki", 999,1743,"73456456"));
        books.add(new Book("pan zbigniew", "Maciek", "Milosz", 666,2015,"83756494"));

        //za pomoca petli for tak to sie wywoluje

//        for (Book b: books) {
//            if(b.getPages() > 500) {
//                System.out.println(b);
//            }
//        }

        books.stream()
                .filter(b -> b.getPages() < 1000)
                .forEach(System.out::println);

        List<Book> booksUnder500 = books.stream()
                .filter(b -> b.getPages() < 500)
                .collect(Collectors.toList());

        System.out.println("ksiazki ponizej 500");
        booksUnder500.stream()
                .forEach(System.out::println);

        //----------------------------------------
        System.out.println("ksiazki posortowane");
        Collections.sort(books, new BookComparator());

        books.forEach(System.out::println);
//--------------------------------------
        System.out.println("na inny sposob z lambda");
        Collections.sort(books, (b1,b2) -> b1.getTitle().compareTo(b2.getTitle()));
        books.forEach(System.out::println);
//---------------------------------------------czysta lambda
        books.stream()
        .sorted((b1,b2) -> b1.getTitle().compareTo(b2.getTitle()))
                .forEach(System.out::println);
//to samo co na gorze ale innaczej
        books.stream()
                .sorted(Comparator.comparing(Book::getTitle))
                .forEach(System.out::println);
//----------------------------------------

        System.out.println(books.stream()
        .anyMatch((b) -> b.getPublishYear() < 1999));

//wypisac wszystkie ksiazki gdzie autorem jest kobieta

        books.stream()
                .filter((b) -> b.getAuthorName().endsWith("a"))
                .forEach(System.out::println);

        List<String> names = books.stream()
                .map( b -> b.getAuthorName())
                .distinct()    //to sprawdza czy sa duplikaty przy metodzie equals jak sa to wywala
                .sorted((a,b) -> a.compareTo(b))
                .collect(Collectors.toList());

        names.forEach(System.out::println);
//-----------------------------------------------------

        List<Library> libraries = new ArrayList<>();
        libraries.add(new Library("Warszawa", books.subList(0,2)));
        libraries.add(new Library("Kraków", books.subList(2,5)));

        //stream ksiazek ze streamu bibliotek
        System.out.println("stream ksiazek ze streamu bibliotek");
        libraries.stream()
                .map(l -> l.getBooks())
                .flatMap(b -> b.stream())
                .forEach(System.out::println);

        System.out.println("ksiazki tylko ze streamu Warszawy");
        libraries.stream()
                .filter(l -> l.getCity().equals("Warszawa"))
                .map(l -> l.getBooks())
                .flatMap(b -> b.stream())
                .forEach(System.out::println);
//---------------------------------------------



    }
}
