package pl.sdacademy.lambda;

import pl.sdacademy.lambda.model.Computable;
import pl.sdacademy.lambda.model.StringPerformance;

public class FunctionalMain {
    public static void main(String[] args) {

        Computable adder = (a,b) -> a + b;
        Computable substract = (a,b) -> a * b;


        System.out.println(adder.compute(1,2));
        System.out.println(substract.compute(1,2));

        //klasa abstrakcyjna ktora jest tworzona, to jest to samo co to na gorze
        Computable adder2 = new Computable() {
            @Override
            public int compute(int a, int b) {
                return a + b;
            }
//---------------------------------------------------------------
        };
//        String s = "lol";
//        char[] tablica = s.toCharArray();
//        for (char c: tablica) {
//            System.out.println(c);
//        }

        StringPerformance deleteEverySecondCharacter = (s) -> {
            char[] tablica = s.toCharArray();
            StringBuilder newString = new StringBuilder();

            for (int i = 0; i<tablica.length; i++) {
                if(i % 2 == 0) {
                    newString.append(tablica[i]);
                }
            }
            return newString.toString();
        };

        System.out.println(deleteEverySecondCharacter.perform("lsdfdsfdsfol"));
//-------------------------------------------------

        StringPerformance returnNumberOfVowels = (s) -> {
            char[] vowels = {'a', 'e', 'i', 'o', 'u', 'y'};
            char[] tablica = s.toLowerCase().toCharArray();
            Integer returnNumber = 0;

            for (int i = 0; i<tablica.length; i++) {
                for(int j=0; j<vowels.length;j++){
                    if(vowels[j] == tablica[i]){
                        returnNumber += 1;
                    }
                }
            }

            return returnNumber.toString();

        };

        System.out.println(returnNumberOfVowels.perform("adamaaaaAOdfVSQeqDee"));


    }
}
