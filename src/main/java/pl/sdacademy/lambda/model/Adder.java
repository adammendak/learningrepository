package pl.sdacademy.lambda.model;

public class Adder implements Computable {

    @Override
    public int compute(int a, int b) {
        return a + b;
    }

    @Override
    public String toString() {
        return "Adder{}";
    }
}
