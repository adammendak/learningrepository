package pl.sdacademy.lambda.model;

public class Mulitplier implements Computable {

    @Override
    public int compute (int a, int b) {
        return a * b;
    }

    @Override
    public String toString() {
        return "Mulitplier{}";
    }
}
