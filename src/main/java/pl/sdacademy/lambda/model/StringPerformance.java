package pl.sdacademy.lambda.model;

@FunctionalInterface
public interface StringPerformance {

    String perform(String str);

}
