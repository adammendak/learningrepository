package pl.sdacademy.lambda.model;

@FunctionalInterface
public interface Computable {
    int compute(int a, int b);

//przyklad metody defaultowej ktora ma cialo
    default String getType() {
        return getClass().getName();
    }
}
