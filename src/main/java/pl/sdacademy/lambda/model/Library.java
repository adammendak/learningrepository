package pl.sdacademy.lambda.model;

import java.util.List;

public class Library {
    private String city;
    private List<Book> books;

    public Library(String city, List<Book> books) {
        this.city = city;
        this.books = books;
    }

    @Override
    public String toString() {
        return "Library{" +
                "city='" + city + '\'' +
                ", books=" + books +
                '}';
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public List<Book> getBooks() {
        return books;
    }

    public void setBooks(List<Book> books) {
        this.books = books;
    }


}
