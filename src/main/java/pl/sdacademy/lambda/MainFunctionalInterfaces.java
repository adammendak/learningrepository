package pl.sdacademy.lambda;

import pl.sdacademy.lambda.model.Book;
import pl.sdacademy.lambda.model.Library;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.function.*;
import java.util.stream.Collectors;

public class MainFunctionalInterfaces {
    public static void main(String[] args) {

        Function <String, Integer> returnStringLength =
                (s) -> s.length();

        System.out.println(returnStringLength.apply("dupa"));

        Function <Library, List<Book>> returnBooks =
                (lib) -> {
                    return lib.getBooks().stream()
                            .filter(b -> b.getAuthorSurname().equals("Mickiewicz"))
                            .collect(Collectors.toList());
                };

        returnBooks.apply(new Library("Warszawa",
                Arrays.asList(new Book("pan tadeka", "Adam", "Mickiewicz", 500,2002,"12341234"),
                        new Book("pan maciek", "Roman", "Michnik", 432,1986,"7243654"),
                        new Book("pan slawek", "Slawek", "Malysz", 162,1943,"13245324"),
                        new Book("pan lukasz", "Zenon", "Slowacki", 999,1743,"73456456"))))
                .forEach(System.out::println);

        //------------------------------- czy string jest even czy odd
        Predicate<String> stringIsEven = (s) -> s.length() % 2 == 0;

        System.out.println(stringIsEven.test("asad"));
        //------------------------------interface consumer

        Book buk = new Book ("Potpo", "Henryk", "Sienkiewicz", 1234,1234,"asd");
        Consumer<Book> bookConsumer = (b) -> b.setIsbn("666");

        bookConsumer.accept(buk);
        System.out.println(buk);
        //------------------------------------Supplier

        Supplier<String> idRandomGeneraor =
                () -> UUID.randomUUID().toString()
                .replaceAll("-","");
        System.out.println(idRandomGeneraor.get());
        System.out.println(idRandomGeneraor.get());
        System.out.println(idRandomGeneraor.get());
        System.out.println(idRandomGeneraor.get());
        System.out.println(idRandomGeneraor.get());

        //--------------bifunction przyjmuje 2 argumenty jakiegos typu i zwraca trzeci
        BiFunction<Integer, String, String> DualComboFuncionZiom =
                (a,b) -> {
                StringBuilder sb = new StringBuilder();
                for(int i = 0; i < a; i++) {
                    sb.append(b);
                }
                return sb.toString();
                };

        System.out.println(DualComboFuncionZiom.apply(5,"hej"));
        //--------------------------------------------
        BinaryOperator<Integer> sum = (a,b) -> a + b;
        System.out.println(sum.apply(10,15));
        //-------------------------------------------
        BinaryOperator<String> binaryExample = (a,b) -> a.concat(b);
        System.out.println(binaryExample.apply("dupa", "niedupa"));

    }
}
