package com.sdacademy.advanced.model;

import java.util.concurrent.TimeUnit;

public class Drummer extends Musician {

    private String name;
    private String instrument;

    public Drummer(String name, String instrument) {
        this.name = name;
        this.instrument = instrument;
    }

    public void play() {
        System.out.println("bum bum");
        for(int i=0;i<10;i++){
            try {
                TimeUnit.SECONDS.sleep(1);
                System.out.println("bum bum bum");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}
