package com.sdacademy.advanced.model;

import java.util.concurrent.TimeUnit;

public class Singer extends Musician {

    private String name;
    private String instrument;

    public Singer(String name, String instrument) {
        this.name = name;
        this.instrument = instrument;
    }

    public void play() {
        for(int i=0;i<10;i++){
            try {
                TimeUnit.SECONDS.sleep(1);
                System.out.println("la la la ");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
