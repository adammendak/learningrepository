package com.sdacademy.advanced.model;

import java.util.ArrayList;
import java.util.List;

public class Orchestra {

    private List<Musician> musicians = new ArrayList<>();

    public List<Musician> addMusician(Musician musician) {
        this.musicians.add(musician);
        return musicians;
    }

    public void concert() {
//        for (Musician musician: this.musicians) {
//            Runnable task = () -> {
//                musician.play();
//            };
//            new Thread(task).start();
//        }

        this.musicians.stream().forEach(m -> {
            Runnable task = () -> {
                m.play();
            };
            new Thread(task).start();
        });

    }

    public static void main(String[] args) {

        Orchestra uorkiestra = new Orchestra();

        uorkiestra.addMusician(new Drummer("Stefan", "drum"));
        uorkiestra.addMusician(new Violinist("Maciek", "violin"));
        uorkiestra.addMusician(new Singer("Jola", "human"));

        uorkiestra.concert();


    }



}

