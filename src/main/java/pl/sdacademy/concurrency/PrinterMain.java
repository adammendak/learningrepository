package pl.sdacademy.concurrency;

import pl.sdacademy.concurrency.model.Printer;
import pl.sdacademy.concurrency.model.PrinterThread;

public class PrinterMain {
    public static void main(String[] args) {
        Printer printer = new Printer();

        PrinterThread t1 = new PrinterThread(printer, "watek1");
        PrinterThread t2 = new PrinterThread(printer, "watek2");

        t1.start();
        t2.start();
    }
}
