package pl.sdacademy.concurrency;

import pl.sdacademy.concurrency.model.MyThread;
import pl.sdacademy.concurrency.model.MyThreadRunnable;

public class Main {

    public static void main(String[] args) {

        MyThread tab[] = new MyThread[10];

        for(int i =0; i<10; i++) {
            tab[i] = new MyThread("watek" + i);
        }

        for(int i=0; i<10; i++) {
            tab[i].start();
            try {
                tab[i].join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

//        MyThread t = new MyThread("asdf");
//        t.start();
//

        MyThreadRunnable tr = new MyThreadRunnable("watek runnable");
        Thread th = new Thread(tr);
        th.start();
//      to samo
        //        Thread th= new Thread(new MyThreadRunnable("watek")).start();

        new Thread(
                () -> System.out.println("wykonuje watek lambda"))
                .start();

        new Thread(() ->
        {
            for(int i=0; i <10; i++) {
                System.out.println(i + "watek lambda");
            }
            System.out.println("wykonuje watek lambda 2 ");
        })
                .start();




//
//        MyThread watek1 = new MyThread("pierwszyWatek");
//        MyThread watek2 = new MyThread("drugiWatek");
//        MyThread watek3 = new MyThread("trzeciWatek");


//
//        watek1.start();
//        watek2.start();
//        watek3.start();


    }
}
