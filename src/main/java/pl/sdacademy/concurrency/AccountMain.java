package pl.sdacademy.concurrency;

import pl.sdacademy.concurrency.model.Account;

public class AccountMain {
    public static void main(String[] args) {
        Account account = new Account(1000);

        new Thread(() -> {

            account.withdrawal(2000);

        }).start();

        new Thread(() -> {
           account.deposit(1500);

        }).start();



    }
}
