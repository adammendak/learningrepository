package pl.sdacademy.concurrency.model;

public class Account {

    private int amount;

    public Account(int amount) {
        this.amount = amount;
    }

    public synchronized void withdrawal(int amount){

        while(this.amount <= amount) {
            try{
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        this.amount -= amount;  
        System.out.println("Wyplata zakonczona sukcesem. Stan konta: " + this.amount);
    }

    public synchronized void deposit(int amount) {
        this.amount += amount;
        System.out.println("Zasilono konto kwota: " + amount + ". Stan konta " + this.amount);
        notify();
    }

    public void payment(int amount) {

    }

    public int getAmount() {
        return amount;
    }
}
