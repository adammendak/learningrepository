package pl.sdacademy.concurrency.model;

public class MyThreadRunnable implements Runnable {

    private String name;

    public MyThreadRunnable(String name) {
        this.name = name;
    }

    @Override
    public void run() {
        for(int i =0; i<10;i++ ) {
            System.out.println(name + " " + i);
        }
    }
}
