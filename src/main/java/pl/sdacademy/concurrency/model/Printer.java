package pl.sdacademy.concurrency.model;

public class Printer {

    public void print(String s) {
        System.out.println("poza blokiem synchronizowanym" + s);

        synchronized (this) {
            for(int i=0; i<10;i++) {
                System.out.println(i + " " + s);
            }
        }
    }

//    public synchronized void print(String s) {
//        for(int i=0; i<10;i++) {
//            System.out.println(s + "wyswietenie " + i);
//        }
//    }

}
