package pl.sdacademy.inputoutput;

import java.io.*;

public class Main {
    public static void main(String[] args) {
//        FileWriter writer = null;
//        try {
////            FileOutputStream fas = new FileOutputStream("/home/adammendak/Workspace/WprowadzenieCzescDruga/iotest.txt");
//            writer = new FileWriter("/home/adammendak/Workspace/WprowadzenieCzescDruga/iotest.txt");
//
////            OutputStreamWriter writer = new OutputStreamWriter(writer);
//            writer.write("DUPA");
//        }
//        catch (FileNotFoundException e) {
//            e.printStackTrace();
//        }
//        catch (IOException e) {
//            e.printStackTrace();
//        }
//        finally {
//            if(writer != null) {
//                try{
//                    writer.close();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//
//            }
//           to jest try with resources, dzieki temu mamy pewnosc absolutna ze writer zostanie zamkniety bo jest interfejs AUTOCLOSABLE
            try(FileWriter writer = new FileWriter("/home/adammendak/Workspace/WprowadzenieCzescDruga/iotest.txt")) {
                writer.write("DUPA2");
                writer.write("DUPA12132");
                writer.write("DUPA2123");
                writer.write("DUPA111112");
            }
            catch (IOException e) {
                e.printStackTrace();
            }

            try(BufferedReader reader = new BufferedReader(new FileReader("/home/adammendak/Workspace/WprowadzenieCzescDruga/iotest.txt"))) {
//                System.out.println(reader.readLine());
                String line;
                while((line = reader.readLine()) != null) {
                    System.out.println(line + "\n");
                }
            }
            catch (IOException e) {
                e.printStackTrace();
            }

        }
    }