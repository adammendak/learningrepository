package pl.sdacademy.projects.Util.Model;


public interface MyList<T> extends MyCollections<T>, Iterable<T>{

    void add(int index, T t);

    boolean remove(int index);

    T get (int index);

}
