package pl.sdacademy.projects.Util.Model;

import java.util.Arrays;
import java.util.Iterator;
import java.util.stream.Stream;

public abstract class MyAbstractDynamicArray<T> {

    protected T array[];

    protected int index = 0;

    protected int maxIndex;

    public MyAbstractDynamicArray() {
        this.array = (T[]) new Object[2];
        this.maxIndex = 1;
    }

    public MyAbstractDynamicArray(int initialSize){
        this.array = (T[]) new Object[initialSize];
        this.maxIndex = initialSize - 1;
    }



    protected void ensureCapacity() {
        if (index > maxIndex){
            // rozszerz tablicę
            array = Arrays.copyOf(array, (maxIndex + 1) * 2);
            maxIndex = array.length - 1;
            System.out.println("Zmieniono długość tablicy na " + array.length);
        }
    }




}
