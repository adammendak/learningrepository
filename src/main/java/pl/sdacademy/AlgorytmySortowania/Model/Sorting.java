package pl.sdacademy.AlgorytmySortowania.Model;

import java.util.Arrays;

public class Sorting {


    public static int[] bubbleSort(int[] arrayToSort) {
        int tmp;

        while (checkIfSorted(arrayToSort) == false) {
            for (int i = 1; i < arrayToSort.length; i++) {
                if (arrayToSort[i] < arrayToSort[i - 1]) {
                    tmp = arrayToSort[i - 1];
                    arrayToSort[i - 1] = arrayToSort[i];
                    arrayToSort[i] = tmp;
                }
            }
        }

        return arrayToSort;
    }

    public static int[] sortByChoice(int[] arrayToSort) {

        int min;
        int index;

        for(int i=0; i < arrayToSort.length; i++) {

            min = arrayToSort[i];
            index = i;

            for(int j=i; j<arrayToSort.length;j++) {
                if(min > arrayToSort[j]){
                    arrayToSort[j] = min;
                    index = j;
                }
            }

           arrayToSort[index] = arrayToSort[i];
            arrayToSort[i] = min;

        }


        return arrayToSort;
    }


    public static boolean checkIfSorted(int[] arrayToSort) {
        for (int i = 1; i<arrayToSort.length;i++) {
            if(arrayToSort[i] < arrayToSort[i-1]) {
                return false;
            }
        }
        return true;
    }

    public static boolean checkIfMin(int[] arrayToSort) {
        int min = arrayToSort[0];
        for(int i = 1; i<arrayToSort.length;i++) {
            if(arrayToSort[i] < min) {
                return false;
            }
        }
        return true;
    }

    public static int binarySearch(int numberToLookFor, int[] arrayToSort) {
        int start = 0;
        int stop = arrayToSort.length;
        int middle = arrayToSort.length/2;

        while(start <= stop) {

            if(arrayToSort[middle] < numberToLookFor) {
                start = middle + 1;
            } else if (arrayToSort[middle] == numberToLookFor) {
                return middle;
            } else {
                stop = middle -1;
            }

            middle = (start + stop)/2;
        }
        return -1;
    }





}
