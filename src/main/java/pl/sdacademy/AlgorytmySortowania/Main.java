package pl.sdacademy.AlgorytmySortowania;

import pl.sdacademy.AlgorytmySortowania.Model.Sorting;

import javax.swing.*;
import java.util.Arrays;

public class Main {

    public static void main(String[] args) {

        int[] testArray = new int[]{1,2,7,3,9,10};

//            Sorting.bubbleSort(testArray);
              Sorting.sortByChoice(testArray);
        Arrays.stream(testArray).forEach(System.out::println);
        System.out.println("--------------------");

        System.out.println(Sorting.binarySearch(10,testArray));
    }
}
