package pl.sdacademy.exceptions.excercises.model;

public class NumberChecker {
    /**
     * this method checks if this integer is a string
     * @param number
     * @throws NumberFormatException
     */

    public void isNumber(String number) throws NumberFormatException{
        try {
            Integer.valueOf(number);
            System.out.println("is a number " + number);
        }
        catch (NumberFormatException e){
          throw new MyRuntimeException("Cannot parse " + number, e);
        }
    }
}
