package pl.sdacademy.exceptions.excercises.model;

public class MyRuntimeException extends RuntimeException {

    public MyRuntimeException(String message, Throwable cause) {
        super(message, cause);
    }

    public MyRuntimeException(String message) {
        super(message);
    }


}
