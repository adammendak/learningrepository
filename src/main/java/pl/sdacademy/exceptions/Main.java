package pl.sdacademy.exceptions;

import java.io.EOFException;
import java.io.FileNotFoundException;

public class Main {
    public static void main(String[] args) {
        try {
//            throwsUnchecked();
            doSomething();
        }
        catch (NullPointerException e) {
            System.out.println(e.getMessage());
        }
        catch (ArithmeticException e) {
            System.out.println(e.getMessage());
        }
        catch(FileNotFoundException e) {
            e.printStackTrace();
        }
        catch (EOFException e) {
            e.printStackTrace();
        }
        finally {
            System.out.println("kuniec");
        }
//        throwChecked();

        }
    public static void throwsUnchecked() {
        if(true) {
            throw new ArithmeticException("zjebalo sie");
        }
        Integer i = null;
        i.toString();
        }

    public static void doSomething() throws FileNotFoundException, EOFException {

        throwChecked();
        if(true) {
            throw new EOFException();
        }
    }

    public static void throwChecked() throws FileNotFoundException {
        try {
            throw new FileNotFoundException();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
