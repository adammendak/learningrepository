package com.sdacademy.advanced.model;

import java.util.concurrent.TimeUnit;

public class Singer extends Musician {

    private String name;
    private String instrument;

    public Singer(String name, String instrument) {
        this.name = name;
        this.instrument = instrument;
    }

    public void play() {

        Runnable task = () -> {
            for(int i=0;i<10;i++){
                try {
                    TimeUnit.SECONDS.sleep(3);
                    System.out.println("la la la ");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        new Thread(task).start();

    }
}
