package com.sdacademy.advanced.model;

public abstract class Musician {

    private String name;

    private String instrument;

    public abstract void play();

}
