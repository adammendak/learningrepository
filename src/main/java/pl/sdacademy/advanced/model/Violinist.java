package com.sdacademy.advanced.model;

import java.util.concurrent.TimeUnit;

public class Violinist extends Musician {

    public String name;
    private String instrument;

    public Violinist(String name, String instrument) {
        this.name = name;
        this.instrument = instrument;
    }

    public void play() {

        Runnable task = () -> {
            for(int i=0;i<10;i++){
                try {
                    TimeUnit.SECONDS.sleep(1);
                    System.out.println("skrzypu skrzypu ");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        new Thread(task).start();

    }

}
