package nauka.model;

import java.util.EmptyStackException;
import java.util.LinkedList;

public class GenericStack<T> implements Stack<T> {

    private LinkedList<T> list = new LinkedList<>();

    @Override
    public void push(T number) {
        list.add(number);
    }

    @Override
    public T pop() {
        if(!list.isEmpty()) {
            return list.pollLast();
        }
        else {
            throw new EmptyStackException();
        }
    }

    @Override
    public T peek() {
        return list.peekLast();
    }

    @Override
    public boolean isEmpty() {
        return list.isEmpty();
    }

    @Override
    public int size() {
        return list.size();
    }
}
