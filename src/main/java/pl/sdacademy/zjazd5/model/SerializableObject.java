package nauka.model;

import java.io.Serializable;

public class SerializableObject implements Serializable {
    private String name;
    private Integer year;

    public SerializableObject() {    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }
}
