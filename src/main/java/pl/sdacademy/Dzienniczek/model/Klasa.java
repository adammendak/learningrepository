package pl.sda.pracaDomowa.Dzienniczek.model;

import java.sql.Struct;
import java.util.ArrayList;
import java.util.Iterator;

public final class Klasa {
    private String nameOfclass;
    private ArrayList<Student> students = new ArrayList<>();
    private Teacher teacher;

    public Klasa(String nameOfclass, Teacher teacher) {
        this.nameOfclass = nameOfclass;
        this.teacher = teacher;
    }

    public String getNameOfclass() {
        return nameOfclass;
    }

    public void setNameOfclass(String nameOfclass) {
        this.nameOfclass = nameOfclass;
    }

    public void iterateStudents() {
        Iterator itr = students.iterator();
        while(itr.hasNext()) {
            Student stu = (Student)itr.next();
            System.out.println(stu.getFirstName() + " " + stu.getLastName());
        }

    }

    public ArrayList<Student> getStudents() {
        return students;
    }
    public Teacher getTeacher() {
        return teacher;
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }

    public void addStudent(Student student) {
        this.students.add(student);
    }

}
