package pl.sda.pracaDomowa.Dzienniczek.model;

public abstract class User {
    protected String firstName;
    protected String lastName;

    public User (String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;

    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

}
