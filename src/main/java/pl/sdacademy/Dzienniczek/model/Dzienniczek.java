package pl.sda.pracaDomowa.Dzienniczek.model;


import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public final class Dzienniczek {
    private String className;
    private Teacher teacher;
    private ArrayList<Student> studentsOfClass = new ArrayList<>();
    private Map<Student, ArrayList<Integer>> grades = new HashMap<>();

    public Dzienniczek(String className, Teacher teacherName, ArrayList<Student> students) {
        this.className = className;
        this.teacher = teacherName;
        this.studentsOfClass.addAll(students);
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public Teacher getTeacherName() {
        return teacher;
    }

    public void addGrades(Student student, ArrayList<Integer> grades) {
        this.grades.put(student, grades);
    }

    public ArrayList<Integer> getGradesOfStudent(Student stu) {
        return grades.get(stu);
    }

    public void setTeacherName(Teacher teacherName) {
        this.teacher = teacherName;
    }

    public void iterateStudents() {
        Iterator itr = studentsOfClass.iterator();
        while(itr.hasNext()) {
            Student stu = (Student)itr.next();
            System.out.println(stu.getFirstName() + " " + stu.getLastName());
        }
    }


}
