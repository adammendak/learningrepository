package pl.sda.pracaDomowa.Dzienniczek;

import pl.sda.pracaDomowa.Dzienniczek.model.*;
import pl.sda.pracaDomowa.Dzienniczek.service.DzienniczekService;

import java.util.ArrayList;

public class App {
    public static void main (String[] args) {

        Student student1 = new Student("adam", "mendak", "3c", "dziennik3c");
        Student student2 = new Student("maciek", "kowalski", "3c", "dziennik3c");
        Student student3 = new Student("tomek", "nowak", "3a", "dziennik3a");
        Student student4 = new Student("marcin", "costam", "3a", "dziennik3a");

        Teacher teacher3c = new Teacher("kasia", "kowal","3c");
        Teacher teacher3a = new Teacher("monika", "kowalsa","3a");

        Klasa trzeciaC = new Klasa("3c", teacher3c);
        Klasa trzeciaA = new Klasa("3a", teacher3a);
        trzeciaC.addStudent(student1);
        trzeciaC.addStudent(student2);
        System.out.println("studenci klasy 3c: ");
        trzeciaC.iterateStudents();
        System.out.println("\n");

        trzeciaA.addStudent(student3);
        trzeciaA.addStudent(student4);
        System.out.println("studenci klasy 3a: ");
        trzeciaA.iterateStudents();

        Dzienniczek dziennik3a = new Dzienniczek("3a", trzeciaA.getTeacher(), trzeciaA.getStudents());
        Dzienniczek dziennik3c = new Dzienniczek("3c", trzeciaC.getTeacher(), trzeciaC.getStudents());

        ArrayList<Integer> oceny = new ArrayList<>();
        oceny.add(1);
        oceny.add(2);
        oceny.add(5);
        oceny.add(2);
        dziennik3a.addGrades(student3,oceny);

        DzienniczekService averageGrades = new DzienniczekService();
        System.out.println(averageGrades.getAverageGradeOfStudent(dziennik3a, student3));

        if(averageGrades.IfTheStudentIsInDangerOfFailing(dziennik3a, student3)) {
            System.out.println("uczen jest zagrozony");
        } else {
            System.out.println("uczen nie jest zagrozony");
        }

        System.out.println(averageGrades.GetAllBadMarks(dziennik3a, student3));

    }
}
