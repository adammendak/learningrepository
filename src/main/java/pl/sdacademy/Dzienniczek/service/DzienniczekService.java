package pl.sda.pracaDomowa.Dzienniczek.service;


import pl.sda.pracaDomowa.Dzienniczek.model.Dzienniczek;
import pl.sda.pracaDomowa.Dzienniczek.model.Student;

import java.util.ArrayList;

public class DzienniczekService {

    public double getAverageGradeOfStudent(Dzienniczek dziennik, Student stu) {
        ArrayList<Integer> grades = dziennik.getGradesOfStudent(stu);
        int sum = 0;
        for(int i=0; i<grades.size(); i++) {
            sum += grades.get(i);
        }
        return (double) sum/grades.size();
    }

    public boolean IfTheStudentIsInDangerOfFailing(Dzienniczek dziennik, Student stu) {
        if(getAverageGradeOfStudent(dziennik, stu) < 2.0){
            return true;
        } else {
            return false;
        }
    }

    public ArrayList<Integer> GetAllBadMarks(Dzienniczek dziennik, Student stu) {
        ArrayList<Integer> grades = dziennik.getGradesOfStudent(stu);
        ArrayList<Integer> returnArray = new ArrayList<>();
        for(int i =0; i<grades.size(); i++) {
            if(grades.get(i) < 2) {
                returnArray.add(grades.get(i));
            }
        }
        return returnArray;
    }
}
