package pl.sdacademy;


public class MyObserver implements Observer {
	private String name;
	
	
	@Override
	public void update(String s) {

		System.out.println("name: " + name  + " new contens: " + s);

	}

	MyObserver(String name) {

		this.name = name;

	}
}
