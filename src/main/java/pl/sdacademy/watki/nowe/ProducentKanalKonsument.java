package pl.mije.sda.java.watki.nowe;

public class ProducentKanalKonsument {

    public static void main(String[] args) {
        Producent producent = new Producent();
        Kanal kanal = new Kanal();
        Konsument konsument = new Konsument();

        producent.produkuj(kanal);
        konsument.konsumuj(kanal);

    }

}

class Producent {

    String[] produkty;

    public void produkuj(Kanal kanal) {
        new Thread(()-> {
        for (String produkt: produkty) {
            if(kanal.czyZajety()) {
                kanal.wystaw(produkt);
            }
        }
    }).start();

}

class Kanal {

    private String produkt;

    void wystaw(String produkt) {
        this.produkt = produkt;
    }

    String pobierz() {
        return null;
    }

    boolean czyZajety() {
        return false;
    }

}

class Konsument {


}