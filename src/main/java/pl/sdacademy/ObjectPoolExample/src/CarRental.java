import javax.smartcardio.CardPermission;
import java.util.LinkedHashMap;
import java.util.Map;

public class CarRental {

    static private CarRental pool;

    private Map<String, Boolean> reusablePool = new LinkedHashMap<>();

    public static CarRental getInstance() {
        if (null == pool) {
            pool = new CarRental();
        }
        return pool;
    }

    public String acquireReusable() {
        String result = null;
        for (Map.Entry<String, Boolean> entry: reusablePool.entrySet()) {
            if (entry.getValue() == false) {
                result = entry.getKey();
                entry.setValue(true);
                break;
            }
        }
        return  result;
    }

    public void releaseReusable(String reusasble) {
        if (reusablePool.containsKey(reusasble)) {
            reusablePool.put(reusasble, false);
        }

    }

    void addObjectToPoll(String reusable) {
        System.out.println("Agregacja");
        reusablePool.put(reusable, false);
    }

    private CarRental() {
        super();
        // bloki
        System.out.println("Inside constructor ReusablePool");

        System.out.println("Kompozycja");
    }

}
