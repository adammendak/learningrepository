package com.sdacademy.design_patterns.model;

public class CountryFactory {

    public static String polandCode = "pl";
    public static String germanyCode = "de";
    public static String franceCode = "fr";

    static public Country getCountry(String countryCode) {
        if(countryCode.equals(polandCode)) {
            return new Country ("Warsaw", 40000000);
        } else if (countryCode.equals(germanyCode)) {
            return new Country( "Berlin", 60000000);
        } else if (countryCode.equals(franceCode)) {
            return new Country("Paris", 6700000);
        }
        return null;
    }
}
