package com.sdacademy.design_patterns.model;

public class Country {
    //agregacja
    private String capitol;
    private long numberOfPeople;

    public Country(String capitol, long numberOfPeople) {
        this.capitol = capitol;
        this.numberOfPeople = numberOfPeople;
    }

    public String getCapitol() {
        return capitol;
    }

    public long getNumberOfPeople() {
        return numberOfPeople;
    }

    @Override
    public String toString() {
        return "Country{" +
                "capitol='" + capitol + '\'' +
                ", numberOfPeople=" + numberOfPeople +
                '}';
    }
}

//zarządzanie cyklem życia
class PolandCountry {
    //kompozycja
    private String capitol = "Warsaw";

    public PolandCountry() {
    }

    public String getCapitol() {
        return capitol;
    }
}

class CountryUtils {
    //asocjacja
    public void prettyPrint(Country country) {
        //
    }
}