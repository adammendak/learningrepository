package com.sdacademy.design_patterns;

import com.sdacademy.design_patterns.model.Country;
import com.sdacademy.design_patterns.model.CountryFactory;

import java.util.Optional;
import java.util.function.Consumer;

public class Main {

    public static void main(String[] args) {
//        Optional <Country> someCountry = Optional.empty();
//        Country maybePoland = someCountry.get();
//        maybePoland = CountryFactory.getCountry(CountryFactory.franceCode);
//        System.out.println("maybe Poland" + maybePoland);

        Optional<Country> polandCountry = Optional.of(CountryFactory.getCountry(CountryFactory.polandCode));
        System.out.println(polandCountry.toString());

        Country france = CountryFactory.getCountry(CountryFactory.franceCode);
        Optional<Country> franceOptional = Optional.of(france);
        printCountry(franceOptional);

        System.out.println("counry consumer : ");
        printCountryWithLambda(franceOptional);
    }

    public static void printCountry(Optional<Country> country) {
        if(country.isPresent()) {
            System.out.println(country.get().toString());
        } else {
            System.out.println("country is null Panie");
        }
    }

    public static void printWithAnonymousConsumerClass(Optional<Country> country) {
        country.ifPresent(new Consumer<Country>() {
            @Override
            public void accept(Country country) {
                System.out.println(country);            
            }
        });
}

public static void printCountryWithLambda(Optional<Country> country) {
//        if (country != null) {
        //to jest consumer, jezeli jest obecny <></>o jest lambda wiec wtedy wywoluje sie jedyna funkcja w tej lambdzie accept
//            country.ifPresent(new CountryConsumer());
//            }
        country.ifPresent(t -> System.out.println(t));
        country.ifPresent(t -> System.out.println(t.getCapitol()));
    }
}

class CountryConsumer implements Consumer<Country> {

    @Override
    public void accept(Country country) {
        System.out.println("Country: " + country);
    }
}