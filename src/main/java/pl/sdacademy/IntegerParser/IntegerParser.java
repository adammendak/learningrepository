package pl.sda.pracaDomowa.IntegerParser;

import java.util.ArrayList;

public class IntegerParser{

    private ArrayList<String> listofIntegers = new ArrayList<>();


    public ArrayList<String> getListofIntegers() {
        return listofIntegers;
    }

    public void addIntegersToList() {
        listofIntegers.add("1");
        listofIntegers.add("2");
        listofIntegers.add("3");
        listofIntegers.add("4");
        listofIntegers.add("5");
        listofIntegers.add("6");
        listofIntegers.add("7");
        listofIntegers.add("8");
        listofIntegers.add("9");
        listofIntegers.add("0");
    }

    public boolean checkIfStringIsInteger(String oneToCheck) {

        this.addIntegersToList();
        int lengthOfString = oneToCheck.length();

        for (int i = 0; i < lengthOfString; i++) {

            String integerToCheck = oneToCheck.substring(i, i + 1);

            if (!listofIntegers.contains(integerToCheck)) {
                return false;

            }
        }
        return true;
    }
}