package pl.sda.collections.model;


import java.time.LocalDate;

public class Book {
    private String title;
    private String author;
    private LocalDate dateOfPublish;

    public Book(String title, String author, LocalDate dateOfPublish) {
        this.title = title;
        this.author = author;
        this.dateOfPublish = dateOfPublish;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public LocalDate getDateOfPublish() {
        return dateOfPublish;
    }

    public void setDateOfPublish(LocalDate dateOfPublish) {
        this.dateOfPublish = dateOfPublish;
    }



}
