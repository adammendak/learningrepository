package pl.sda.collections.service;

import pl.sda.collections.model.Book;

import java.util.ArrayList;
import java.util.List;

public class BookService {
    private List<Book> collections;

    public BookService() {
        this.collections = new ArrayList<>();
    }

    public void addNewBook(Book book) {
        this.collections.add(book);
    }

    public List<Book> getAllBooks() {
        return this.collections;
    }

    public Book findBookByTitleAndAuthor(String author, String title) {
        for (Book book: this.collections) {
            if(book.getTitle().equals(title) && book.getAuthor().equals(author)) {return book;}
        }
        return null;
    }

    public Boolean removeBook(Book book) {
        return this.collections.remove(book);
    }

    public Boolean removeBookByAuthor(String author) {
        for(Book book: this.collections) {
            if(book.getAuthor().equals(author)){
                this.collections.remove(book);
                return true;
            }
        }
        return false;
    }

}
