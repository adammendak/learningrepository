package pl.sda.collections.service;

import pl.sda.collections.model.Author;

import java.util.HashSet;
import java.util.Set;

public class AuthorService {
    private Set<Author> authors = new HashSet<>();

    public AuthorService() {
        this.authors = new HashSet<>();
    }

    public void addAuthor(Author author) {
        authors.add(author);
    }

    public Set<Author> findAllAuthors() {
        return this.authors;
    }

    public boolean removeAuthor(Author author) {
        return this.authors.remove(author);
    }
}
