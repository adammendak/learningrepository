package nauka.sdaPodstawy;

import java.lang.String;

public class Pesel {

        private String pesel;

        public Pesel ( String pesel) {
            this.pesel = pesel;
        }

        public void setPesel (String pesel){
            this.pesel = pesel;
        }

        public String getPesel(){
            return pesel;
        }

        public boolean validatePesel() {

            String secondToForthNumber;
            String fifthToSixthNumber;
            String ningthToTenthNumber;


            try{
                long validateIfNumber = Long.parseLong(this.pesel);
            } catch (NumberFormatException e) {
                System.out.println("pesel nie jest numerem");
                return false;
            }

            if(this.pesel.length() != 11 ) {
                if (this.pesel.length() < 11){
                    System.out.println("za krotki pesel");
                } else {
                    System.out.println("za dlugi pesel");
                }
                return false;
            }

            secondToForthNumber = this.pesel.substring(2,4);
            fifthToSixthNumber = this.pesel.substring(4,6);
            ningthToTenthNumber = this.pesel.substring(7,9);

            if ( Integer.parseInt(secondToForthNumber)  > 13 ) {
                System.out.println("niepoprawny miesiac w Pesel");
                return false;
            }

            if(Integer.parseInt(fifthToSixthNumber) >31) {
                System.out.println("niepoprawny dzien w Pesel, nie moze byc wiecej niz 31");
                return false;
            }

            if(Integer.parseInt(ningthToTenthNumber) == 14 ||
                    Integer.parseInt(ningthToTenthNumber) == 18 ||
                    Integer.parseInt(ningthToTenthNumber) == 27) {
            } else {
                System.out.println("niepoprawny numer identyfikacyjny");
                return false;
            };

            if (Integer.parseInt(this.pesel.substring(10,11)) != 9){
                return false;
            }

            return true;

        }


}