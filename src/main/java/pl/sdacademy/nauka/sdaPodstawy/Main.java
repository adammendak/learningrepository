package nauka.sdaPodstawy;

public class Main {

    public static void main (String[] args) {

        int myFirstInt = 100;

        System.out.println("hello world " + myFirstInt++);
        System.out.println(myFirstInt);
        myFirstInt += 1;
        System.out.println(myFirstInt);

        //tablice
        int array[] = new int[3];
        array[0] = 2;
        array[1] = 1;
        array[2] = 5;

        int array2[] = {1,2,3,4};

        System.out.println("first element " + array[0] + " last element " + array[2]);

        for (int element : array) {
            System.out.println("element foreach " + element);
        }

        for (int i=0; i<array.length; i++) {
            System.out.println("for element o indexie " + i + " to " + array[i]);
        }

        int index =0;
        while (index < array.length) {
            System.out.println("for element to " + array[index]);
            index++;
        }

        int [][] multi = new int [2][3];
        multi[0][0] = 1;
        multi[0][1] = 2;
        multi[0][2] = 3;
        multi[1][0] = 4;
        multi[1][1] = 5;
        multi[1][2] = 6;

        System.out.println(multi[0][2] + " " + multi[1][2]);



        int val1 = 9;
        int val2 = 50;
        int val3 = 10;

        int result = val1 > val2 ? val1 : val2;
        if(result < val3)
            result = val3;

        System.out.println("najwiekszy to " + result);



        int arraynew[] = new int[5];
        arraynew[0] = 1;
        arraynew[1] = 41;
        arraynew[2] = 7;
        arraynew[3] = 80;
        arraynew[4] = 10;

        int sum = 0;
        int resultArray = array[0];

        for(int i = 0; i < arraynew.length; i++) {
            if(resultArray < arraynew[i]) {
                resultArray = arraynew[i];
            }
            sum += arraynew[i];
        }

        double srednia = sum / arraynew.length;

        System.out.println("suma to " + sum);
        System.out.println("srednia to " + srednia);
        System.out.println("najwieksza to " + resultArray);

        for (int i = 0; i < arraynew.length; i++) {
            if(arraynew[i] % 2 == 0) {
                System.out.println("parzysta liczba " + arraynew[i]);
            } else {
                System.out.println("nieparzysta liczba to " + arraynew[i]);
            }
        }


        int userAge = 25;

        switch (userAge) {
            case 20:
                System.out.println("User age is: 20");
                break;
            case 25:
                System.out.println("User age is: 25");
                break;
            case 30:
                System.out.println("User age is:30");
                break;
            default:
                System.out.println("User age Not 20, 25 or 30");
        }

        int [][] tablica = new int [3][3];
        tablica[0][0] = 130;
        tablica[0][1] = 122;
        tablica[0][2] = 433;
        tablica[1][0] = 16;
        tablica[1][1] = 1420;
        tablica[1][2] = 67548;
        tablica[2][0] = 977;
        tablica[2][1] = 8685;
        tablica[2][2] = 120;

        int sumaPrzekatna = 0;

        for(int i =0; i<tablica.length;i++){
            for (int j = 0; j<tablica.length; j++){
                if(i == j) {
                    sumaPrzekatna += tablica[i][j];
                }
            }
        }

        int suma2 = 0;

        String indexy = "";


        int liczbaOdpowiadajacych = 0;

        for(int i =0; i<tablica.length;i++){
            for (int j = 0; j<tablica.length; j++){
               if(tablica[i][j] > 500 && tablica[i][j] % 2 ==0){
                   suma2 += tablica[i][j];
                   indexy += " index x- " + i + " index y- " + j + "\n";
                   liczbaOdpowiadajacych++;
               }
            }
        }
        double avg = suma2 / liczbaOdpowiadajacych;

        System.out.println("suma indexow po przekatnej =" + sumaPrzekatna);
        System.out.println("suma drugiego zadania to =" +suma2);
        System.out.println("srednia to" + avg);
        System.out.println("miejsca indexow to \n" + indexy);

        //odwrocic tablice i wypisac najwieksza wartosc

        int [] tablicaDoOdwrocenia = new int [10];
        tablicaDoOdwrocenia[0] = 1;
        tablicaDoOdwrocenia[1] = 2;
        tablicaDoOdwrocenia[2] = 3;
        tablicaDoOdwrocenia[3] = 4;
        tablicaDoOdwrocenia[4] = 75;
        tablicaDoOdwrocenia[5] = 6;
        tablicaDoOdwrocenia[6] = 7;
        tablicaDoOdwrocenia[7] = 8;
        tablicaDoOdwrocenia[8] = 9;
        tablicaDoOdwrocenia[9] = 10;

        int [] odwroconaTablica = new int[10];
        int indexTablicy = 0;
        int najwieksza = tablicaDoOdwrocenia[0];

        for (int i = tablicaDoOdwrocenia.length -1; i >= 0; i--) {
            odwroconaTablica[indexTablicy] = tablicaDoOdwrocenia[i];
            indexTablicy++;
            if(tablicaDoOdwrocenia[i] > najwieksza) {
                najwieksza = tablicaDoOdwrocenia[i];
            }
        }

        for (int liczba : odwroconaTablica) {
            System.out.println(liczba);
        }
        System.out.println("najwieksza wartos to " + najwieksza);

        int k = 0;
        do {

            if(k % 2 == 0) {
                System.out.println(odwroconaTablica[k]);
            }
            k++;
        } while (k < odwroconaTablica.length);

     }
}
