package nauka.sdaPodstawy.validator;

import nauka.sdaPodstawy.domain.User;

public class UserValidator {

    private static final Integer MIN_ALLOWED_USER_AGE = 18;
    private static final Integer VALID_PHONE_NUMBER_LENGTH = 9;
    private static final String EMPTY_PASSWORD = "";

    public static boolean validateAge(User user ){
    return user.getAge() > MIN_ALLOWED_USER_AGE;
    }

    public static boolean validateTelephone(User user) {
        return VALID_PHONE_NUMBER_LENGTH.equals(user.getTelephone().length());
    }

    public static boolean validatePassword(User user) {
        return EMPTY_PASSWORD.equals(user.getPassword()); //to jest metoda na Stringu wiec zwroci true w tym wypadku.

    }
}
