package nauka.sdaPodstawy.domain;

public class User {
    private String login;
    private String password;
    private String firstName;
    private String lastName;
    private int age;
    private String telephone;

    public User (String login, String password, String telephone, int age) {
    this.login = login;
    this.password = password;
    this.age = age;
    this.telephone = telephone;
    }

    public String getLogin() {
        return login;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public static String message;

    public static void printMessage() {
        System.out.println(message);
    }

    public User(){

    }
//    public void setAge(int age){
//        if (age > 18) {
//            this.age = age;
//        } else {
//            System.out.println("dozwolone od lat 18 ");
//        }
//    }

    public int getAge() {
        return this.age;
    }
//
//    public void setTelephone(int telephone) {
//        if (Integer.toString(telephone).length() == 9) {
//            this.telephone = telephone;
//            System.out.println("udalo sie zmienic");
//        } else{
//            System.out.println(" telefon powinien miec 9 cyfr ");
//        }
//    }

    public String getTelephone() {
        return this.telephone;
    }

    public String getPassword(){
        return password;
    }

    public void greetYourself() {
        System.out.println("my login is " + this.login);
        System.out.println("my password is " + this.password);
        System.out.println("my first name is" + this.firstName);
        System.out.println("my last name is " + this.lastName);
        System.out.println("my age is " + this.age);
        System.out.println("my telephone is " + this.telephone);
    }

}
