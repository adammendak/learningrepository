package nauka.sdaPodstawy.domain;

import nauka.sdaPodstawy.validator.UserValidator;

public class Main {

    public static void main (String[] args) {
        User uzytkownik1 = new User("admin", "admin", "123123123", 30);
        User uzytkownik2 = new User ();
        uzytkownik2.message = "dupa";

        User.message = "test";
        System.out.println(User.message);
        uzytkownik1.greetYourself();
        System.out.println(uzytkownik2.message);
        User.printMessage();
        System.out.println("------------------------------");

        //validacja
        if(UserValidator.validateAge(uzytkownik1)){
            System.out.println("age is valid");
        } else {
            System.out.println("age is incorrect");
        }

        if(!UserValidator.validatePassword(uzytkownik1)){
            System.out.println("password is valid");
        } else {
            System.out.println("password is incorrect");
        }

        if(UserValidator.validateTelephone(uzytkownik1)){
            System.out.println("telephone is valid");
        } else {
            System.out.println("telephone is incorrect");
        }



    }

}
