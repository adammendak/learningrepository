package nauka.calculator;

public class Calculator {

    private double val1;
    private double val2;
    private double result;
    private String operation;
//    public double[] historia = new double[5];
//    public int indexAktualnyWpis = 0;

    public Calculator () {   };

    public Calculator(String operation) {
        this.operation = operation;
    }

    public Calculator(double val1, double val2, String operation) {
        this(operation);
        this.val1 = val1;
        this.val2 = val2;
    }

    public void setVal1( double val1) {
        this.val1 = val1;
    }

    public double getVal1() {
        return val1;
    }

    public void setVal2( double val2) {
        this.val2 = val2;
    }

    public double getVal2() {
        return val2;
    }

    public void setOperation( String operation) {
        this.operation = operation;
    }

    public String getOperation() {
        return operation;
    }

    public Boolean endOfWork() {
        switch (operation) {
            case "q":
                return true;
            default:
                return false;
        }
    }

    public Boolean mathOperation() throws IllegalArgumentException{

        switch (operation) {
            case "a":
                result = val1 + val2;
                  System.out.println("wynik to : " + result);
//                historia[indexAktualnyWpis] = result;
//                indexAktualnyWpis++;
                return true;

            case "b":
                result = val1 - val2;
                System.out.println("wynik to : \n" + result);
//                historia[indexAktualnyWpis] = result;
//                indexAktualnyWpis++;
                return true;
            case "c":
                result = val1 * val2;
                System.out.println("wynik to : \n" + result);
//                historia[indexAktualnyWpis] = result;
//                indexAktualnyWpis++;
                return true;
            case "d":
                try {
                    if (val2 == 0)
                        throw new IllegalArgumentException();
                    result = val1 / val2;
                    System.out.println("wynik to : \n" + result);
                } catch (IllegalArgumentException e) {
                    System.out.println("nie mozna dzielic przez 0");
                } finally {
                    return true;
                }
            case "q":
                System.out.print("koniec pracy kalkulatora \n" +
                        "historia ostatnich 5 wpisow: \n");
//                for (double CurrentVal : historia) {
//                    System.out.println(CurrentVal);
//                }
                return false;
            default:
                System.out.println("bledny wpis");
                return true;
        }
    }
}