package nauka.calculator;

import java.util.InputMismatchException;
import java.util.Scanner;



public class Main {

    public static void main(String[] args) {
        Boolean calculatorWorking = true;
        Scanner input = new Scanner(System.in);
        Calculator calculator = new Calculator();

        while (calculatorWorking) {
            System.out.println("podaj typ operacji :\n" +
                    "dodawanie - a \n" +
                    "odejmowanie - b \n" +
                    "mnożenie -c \n" +
                    "dzielenie -d \n" +
                    "skoncz dzialanie programu - q \n");

            calculator.setOperation(input.next());

            if (!calculator.endOfWork()) {

                try {
                    System.out.println("podaj pierwsza cyfre :");
                    calculator.setVal1(input.nextDouble());

                    System.out.println("podaj druga cyfre :");
                    calculator.setVal2(input.nextDouble());

                    calculator.mathOperation();

                } catch (InputMismatchException e) {
                    System.out.println("nie jest to liczba ");
                }
            } else {
                //w przypadku q metoda zwraca wartosc false, która zatrzymuje prace programu
                //i wyswietla 5 ostatnich wpisow w historii kalkulatora

                calculatorWorking = calculator.mathOperation();
            }


        }
    }
}