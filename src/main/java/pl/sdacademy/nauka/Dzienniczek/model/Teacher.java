package pl.sda.pracaDomowa.Dzienniczek.model;


public final class Teacher extends User {
    private String classToTeach;

    public Teacher(String firstName, String lastName, String classToTeach) {
        super(firstName, lastName);
        this.classToTeach = classToTeach;
    }

    public void setClassToTeach(String classToTeach) {
        this.classToTeach = classToTeach;
    }

    public String getClassToTeach() {

        return classToTeach;
    }
}
