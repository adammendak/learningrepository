package pl.sda.pracaDomowa.Dzienniczek.model;

public final class Student extends User {

    private String dzienniczek;
    private String classTowhichBelongs;

    public String getClassTowhichBelongs() {
        return classTowhichBelongs;
    }

    public void setClassTowhichBelongs(String classTowhichBelongs) {
        this.classTowhichBelongs = classTowhichBelongs;
    }

    public Student (String firstName, String lastName, String classToWhichBelongs, String dzienniczek){
        super(firstName, lastName);
        this.dzienniczek = dzienniczek;
        this.classTowhichBelongs = classToWhichBelongs;

    }

    public String getDzienniczek() {
        return dzienniczek;
    }

    public void setDzienniczek(String dzienniczek) {
        this.dzienniczek = dzienniczek;
    }
}
