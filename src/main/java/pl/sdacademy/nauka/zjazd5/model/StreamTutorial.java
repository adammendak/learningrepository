package nauka.model;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class StreamTutorial {
    public static void main(String[] args) {
        Student student5 = Student.builder()
                .lastName("kowalski")
                .firstName("Michal")
                .scores(new ArrayList<>())
                .age(20)
                .build();
        System.out.println(student5);

        Student student1 = new Student("adam", "mendak",  25);
        Student student2 = new Student("maciek", "kowalski",   22);
        Student student3 = new Student("michal", "kowalski", 24);
        Student student4 = new Student("kamil", "kowalski", 26);

        student1.scores.add(1);
        student1.scores.add(1);
        student1.scores.add(5);
        student1.scores.add(6);
        student2.scores.add(4);
        student2.scores.add(2);
        student2.scores.add(5);
        student2.scores.add(1);
        student3.scores.add(2);
        student3.scores.add(5);
        student3.scores.add(2);
        student3.scores.add(1);
        student3.scores.add(1);
        student4.scores.add(5);
        student4.scores.add(5);
        student4.scores.add(4);
        student4.scores.add(5);





        List<Student> students = Arrays.asList(student1,student2, student3, student4);

        Map<Integer, List<Student>> collect1 = students.stream()
                .collect(Collectors.groupingBy(p -> p.getAge()));

        Map<String, Integer> collect2 = students.stream()
                .collect(Collectors.toMap(Student::getFirstName, Student::getAge));

        System.out.println(collect2);

        Map<String, Integer> collect3 = students.stream()
                .collect(Collectors.toMap(Student::getFirstName,
                        Student -> Student.getAvgGrades(Student.getScores())));

        System.out.println(collect3);

        


//        System.out.println(collect1);
//
//        Map<String, Integer> collect =
//

    }
}
