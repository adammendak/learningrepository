package nauka.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.ArrayList;
import java.util.List;

@Data
@EqualsAndHashCode
@Builder
@AllArgsConstructor
public class Student {

    private String firstName;
    private String lastName;
    List<Integer> scores;
    private Integer age;

    public Student(String firstName, String lastName, Integer age) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.scores = new ArrayList<>();
    }

    public static Student of (String firstName, String lastName, List<Integer> integerList, Integer age) {
        return new Student(firstName, lastName, integerList, age);

    }

    public Integer getAvgGrades(List<Integer> grades) {
        int sum = 0;
        for(int i =0; i<grades.size(); i++){
            sum += grades.get(i);
        }
        return sum/grades.size();
    }
}
