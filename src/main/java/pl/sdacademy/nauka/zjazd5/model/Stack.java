package nauka.model;

public interface Stack<T> {
    void push(T number);
    T pop();
    T peek();
    boolean isEmpty();
    int size();
}
