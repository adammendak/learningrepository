package nauka;

import com.sun.org.apache.xpath.internal.SourceTree;
import nauka.model.Box;
import nauka.model.Converter;
import nauka.model.GenericStack;
import nauka.model.SerializableObject;
import sun.rmi.server.InactiveGroupException;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

import static java.time.LocalDate.now;

public class Main {
    public static void main (String[] args) {
        Path path = Paths.get("someText.txt");

        List<String> strings = new ArrayList<>();

        try{
            strings = Files.readAllLines(path);
        } catch (IOException e) {
            e.printStackTrace();
        }

        strings.forEach(System.out::println);
        System.out.println(strings);
        strings.forEach(p -> System.out.println(p));
        //--------------------SERIALIZACJA-----------------------

        SerializableObject something = new SerializableObject();
        something.setName("lol");
        something.setYear(1992);

        try (
                FileOutputStream fileOutputStream =
                        new FileOutputStream("serializowanyObiekt.txt");

                ObjectOutputStream out = new ObjectOutputStream(fileOutputStream);
        ) {
            out.writeObject(something);
        } catch (IOException e) {
            e.printStackTrace();
        }

        //----------------DESERIALIZACJA--------------------

        SerializableObject something1 = null;

        try(
            FileInputStream fileInputStream = new FileInputStream("serializowanyObiekt.txt");
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        ) {
            something1 = (SerializableObject) objectInputStream.readObject();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }

        System.out.println("Deserialized Object: ");
        if(something1 != null) {
            System.out.println(something1.getName());
            System.out.println(something1.getYear());
        }
        //-------------- STACK<T> implementacja------------

        GenericStack<Integer> mystack = new GenericStack<>();
        mystack.push(22);
        mystack.push(24);
        mystack.push(32);
        mystack.push(41);

        System.out.println(mystack.pop());
        System.out.println(mystack.peek());
        System.out.println("wielkosc kolejki: " + mystack.size());
        System.out.println(mystack.pop());
        System.out.println("wielkosc kolejki: " + mystack.size());
        System.out.println(mystack.pop());
    //-------------------parametry ograniczone

        Box<Integer> box = new Box<>();
        Box<Number> box1 = new Box<>();

        Box.of(2);

        Box of = Box.of(2.0f);

        System.out.println(of.getValue());

//------------------------------LAMBDA
        Converter<String, Integer> converter = (from) -> Integer.valueOf(from);
        Integer converted = converter.convert("123");
        System.out.println(converted);

//        LocalDate now = new LocalDate(1992,12,30);
        LocalDate now1 = LocalDate.now();

//        Converter<LocalDate, String> converter1= (from) -> from.toString();
        Converter<LocalDate, String> converter1= LocalDate::toString;
        String converted1 = converter1.convert(now1);
        System.out.println(now1);
        System.out.println(converted1);

        Function<Integer, Integer> addTen = i -> i + 10;

        Integer oneToBeAddedTo = addTen.apply(10);
        System.out.println("wynik pierwszej funkcji " + oneToBeAddedTo);

        Function <Integer, Integer> multiplyByTwo = i -> i * 2;
        Integer oneToBeMultiplied = multiplyByTwo.apply(5);

        System.out.println("wynik drugiej funkcji to :" + oneToBeMultiplied);

        Function <Integer, Integer> addTenAndSout = i -> i +10;
        Function <Integer, String > sout = i -> "wynik to " + i;
        String oneToBeSout = addTenAndSout.andThen(sout).apply(10);
        System.out.println(oneToBeSout);



    }
}
