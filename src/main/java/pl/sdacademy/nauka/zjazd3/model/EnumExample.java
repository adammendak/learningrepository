package nauka.zjazd3.model;

public enum EnumExample {
    ADDITION,
    SUBTRACTION,
    MULTIPLICATION,
    DIVISION;

    public double calculate(double x, double y) {
        switch(this) {
            case ADDITION:
                return x + y;
            case SUBTRACTION:
                return x - y;
            case MULTIPLICATION:
                return x * y;
            case DIVISION:
                return x / y;
            default:
                throw new AssertionError("operation does not exist: " + this);
        }
    }
}
