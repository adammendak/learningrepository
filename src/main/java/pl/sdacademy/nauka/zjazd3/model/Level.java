package nauka.zjazd3.model;

public enum Level {
    HIGH(3),
    MEDIUM(2),
    LOW(1);

    private final int levelCode;
    private final String high = "dangerous level";
    private final String medium = "default level";
    private final String low = "secure level";

    Level(int levelCode) {
        this.levelCode = levelCode;
    }

    public int getLevelCode() {
        return this.levelCode;
    }
    public String getMessage() {
        if(this.getLevelCode() == 3) {
            return high;
        } else if (this.getLevelCode() == 2) {
            return medium;
        } else {
            return low;
        }
    }
}
