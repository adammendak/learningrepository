package nauka.zjazd3.model;

public class Palindrom {

    public static boolean isItPalindrome (String checkIt) {
        int length = checkIt.length() -1;
        for(int i=0; i<length; i++){
            if(checkIt.charAt(i) != (checkIt.charAt(length - i))){
                return false;
            }
        };
        return true;
    }
}
