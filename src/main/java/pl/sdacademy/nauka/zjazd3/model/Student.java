package nauka.zjazd3.model;

public class Student {

    private Double[] grades;

    public Student(Double... grades){
        this.grades = grades;
    }

    public void getGrades() {
        for(int i=0;i<this.grades.length; i++){
            System.out.print(grades[i] + " ");
        }
    }

    public Double getAverageGrades(){
        Double sum = 0.0;
        for (Double number: grades) {
            sum+= number;
        }
        Double average = sum/grades.length;
        System.out.println("srednia to: " + average);
        return average;
    }

    public void gradeStudent(Double average){
        if(average >= 5.0) {
            Grades.WZOROWY.getMessage();
        } else if (average>= 3.0 && average <5.0) {
            Grades.DOBRY.getMessage();
        } else if(average< 3.0) {
            Grades.NIEDOSTATECZNY.getMessage();
        }
    }


}
