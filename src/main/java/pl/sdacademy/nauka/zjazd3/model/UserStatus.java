package nauka.zjazd3.model;

/**
 * Created by adammendak on 29.07.17.
 */
public enum UserStatus {
    ACCEPTED,
    BLOCKED;
}
