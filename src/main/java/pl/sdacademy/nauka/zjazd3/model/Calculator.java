package nauka.zjazd3.model;

public class Calculator {

    private int sum;

    public Calculator(){
        this.sum = 0;
    }

    public int calculateSum(int... numbers) {
        int sum =0;
        for (int i=0; i<numbers.length;i++) {
            sum+=numbers[i];
        }
        return this.sum+=sum;
    }
    public int getSum() {
        return this.sum;
    }
}
