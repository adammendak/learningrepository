package nauka.zjazd3.model;

public enum Grades {
    WZOROWY("uczeń bardzo dobry"),
    DOBRY("uczeń dobry"),
    NIEDOSTATECZNY("uczeń niedostateczny");

    private final String message;

    Grades(String message) {
        this.message = message;
    }

    public void getMessage(){
        System.out.println(this.message);
    }



}
