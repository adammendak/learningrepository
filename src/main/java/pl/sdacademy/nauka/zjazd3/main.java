package nauka.zjazd3;

import nauka.zjazd3.model.*;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class main {
    public static void main (String[] args) {
        double value = EnumExample.ADDITION.calculate(10, 20);
        System.out.println(value);
//-------------------------------------------------------------

        Scanner input = new Scanner(System.in);

        System.out.println("podaj numer dnia tygodnia: ");
        String day = input.next();

        switch (day) {
            case "MONDAY":
                WeekDays.MONDAY.numberOfDayItIs();
                break;
            case "TUESDAY":
                WeekDays.TUESADAY.numberOfDayItIs();
                break;
            case "WEDNESDAY":
                WeekDays.WEDNESDAY.numberOfDayItIs();
                break;
            case "THURSDAY":
                WeekDays.THURSDAY.numberOfDayItIs();
                break;
            case "FRIDAY":
                WeekDays.FRIDAY.numberOfDayItIs();
                break;
            case "SATURDAY":
                WeekDays.SATURDAY.numberOfDayItIs();
                break;
            case "SUNDAY":
                WeekDays.SUNDAY.numberOfDayItIs();
                break;
            default:
                System.out.println("wrong week day");
        }
//---------------------------------------------
        UserStatus userAccepted = UserStatus.ACCEPTED;
        UserStatus userBlocked = UserStatus.BLOCKED;

        if(userAccepted.equals(userBlocked)) {
            System.out.println("they are the same");
        } else {
            System.out.println("not the same ");
        }
//---------------------------------------------------

        System.out.println(Level.MEDIUM.getLevelCode());
        System.out.println(Level.HIGH.getMessage());
        System.out.println(Level.MEDIUM.getMessage());
        System.out.println(Level.LOW.getMessage());
//------------------------------------------------------
        Calculator calculator = new Calculator();
        calculator.calculateSum(1,2,3,4,5,6,7,8);
        System.out.println("suma to " + calculator.getSum());

        int[] numers1 = {1,5,2,3,5,1,2,3,5};
        calculator.calculateSum(numers1);
        System.out.println("suma to " + calculator.getSum() + "\n");
//--------------------------------------------------------

        Student student1 = new Student(1.0,2.0,3.0,4.0);
        Student student2 = new Student(2.0,6.0,6.0,4.0,5.0);
        Student student3 = new Student(5.0,1.0,1.0);

        System.out.println("oceny pierwszego");
        student1.getGrades();
        student1.gradeStudent(student1.getAverageGrades());
        System.out.println("\n");

        System.out.println("oceny drugiego");
        student2.getGrades();
        student2.gradeStudent(student2.getAverageGrades());
        System.out.println("\n");

        System.out.println("oceny trzeciego");
        student3.getGrades();
        student3.gradeStudent(student3.getAverageGrades());
        System.out.println("\n");
//--------------------------------------
        String testString = new String("Dupa dupa Dupa");
        if(testString.startsWith("Dupa")) {
            System.out.println("it does");
        };
        String newString = testString.replaceAll("dupa", "nieDupa");
        System.out.println(newString);
        System.out.println(testString);
        int lol = testString.lastIndexOf("Dupa");
        System.out.println(lol);
        boolean dupalol = testString.contains("dupaa");
        if(dupalol) {
            System.out.println("true");
        }
        String nowy = testString.concat("asdasd");
        System.out.println(nowy);

        if(Palindrom.isItPalindrome("ddad")) {
            System.out.println("jest");
        } else {
            System.out.println("nie jest");
        }
//-------------------------------------------------------
        LocalDateTime currentTime = LocalDateTime.now();
        LocalDate date1 = currentTime.toLocalDate();
        Month month = currentTime.getMonth();
        int days = currentTime.getDayOfMonth();
        int seconds = currentTime.getSecond();
        LocalDateTime date2 = currentTime.withDayOfMonth(10).withYear(2012);
        LocalDate date3 = LocalDate.of(2014, Month.DECEMBER, 12);
        LocalTime date4 = LocalTime.of(22, 15);
        LocalTime date5 = LocalTime.parse("20:15:30");

        System.out.println(date1);
        System.out.println(month);
        System.out.println(days);
        System.out.println(seconds);
        System.out.println(date2);
        System.out.println(date3);
        System.out.println(date4);
        System.out.println(date5);
//-------------------------------------------------------
        //LISTY I KOLEKCJE
        List<String> myList = new ArrayList<String>();

    }
}
