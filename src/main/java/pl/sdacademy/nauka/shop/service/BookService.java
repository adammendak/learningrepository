package nauka.shop.service;


import nauka.shop.model.Product;

public class BookService extends ProductService {
	
	public static final Double BOOK_MARGIN = 10D;
	//Zmienili�my na public i static, �eby w MAIN skorzysta� z BookService.BOOK_MARGIN

	@Override
	public Double calculateProductPriceWithMargin(Product product) {
		return product.getPrice() + BOOK_MARGIN;
	}
	
	
}


//tu mog�oby by� Book book pod warunkiem, 
//�e zamiast product.getPrice() by�aby ca�a metoda calculate z ProductService