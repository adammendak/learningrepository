package nauka.shop.main;

import model.Book;
import repository.BookRepository;
import repository.BookRepositoryImpl;
import repository.Logable;
import service.BookService;

public class Main {

	private static Book[] books = new Book[10];

	public static void main(String[] args) {
		initializeApp();
		
		BookRepository bookRepository = new BookRepositoryImpl(books);
		Book foundBook = bookRepository.findBookById(1);
		System.out.println(foundBook);
		//piszemy kod pod interface, nie pod implementacje
		
		Book newBook = new Book(4, "Title 4", 25D, 10D);
		bookRepository.save(newBook);
		Book book = bookRepository.findBookById(4);
		
		
		//System.out.println(book);
	}
		
/*		Book book = new Book("Pan Kleks", 150D, BookService.BOOK_MARGIN);
		BookService bookService = new BookService ();
		Double productPriceWithMargin = bookService.calculateProductPriceWithMargin(book);
		System.out.println("Book price: " + productPriceWithMargin);
	}
		BookRepository bookRepository = new BookRepositoryImpl();
		BookRepositoryImpl b = (BookRepositoryImpl) bookRepository; //rzutowanie, castowanie
		
		Logable bookRepository1 = new BookRepositoryImpl();
		
		
		Logable bookRepository = new BookRepositoryImpl();
		BookRepositoryImpl b = (BookRepositoryImpl) bookRepository;
*/	
	
	
	private static void initializeApp() {
		initializeBooks();
		//initializeGame i Musik b�dzie
	}
	
	private static void initializeBooks() {
		Book book1 = new Book(1, "Title 1", 100D, 10D);
		Book book2 = new Book(2, "Title 2", 200D, 10D);
		Book book3 = new Book(3, "Title 3", 300D, 10D);
	
		books[0] = book1;
		books[1] = book2;
		books[2] = book3;
	}
	}

