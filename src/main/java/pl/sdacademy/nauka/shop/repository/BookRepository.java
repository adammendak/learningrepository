package nauka.shop.repository;
import nauka.shop.model.Book;

public interface BookRepository extends repository.Logable {
	
	//interface to kontrakt z klientem
	//nie mo�emy tworzy� obiektow interfac�w
	
	void save(Book book);
	
	Book findBookById(Integer id);
	
	Integer delete(Integer id);
	
	Book update(Book book);

}
