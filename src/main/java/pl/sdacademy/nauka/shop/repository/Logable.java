package nauka.shop.repository;

public interface Logable {
	
	void before();
	
	void after();

}
