package nauka.shop.repository;

import nauka.shop.model.Book;

public class BookRepositoryImpl implements repository.BookRepository {
	private Integer IndexOfEmptyBooksPosition;
	private static Book[] books;
	public BookRepositoryImpl(Book[] books) {
		this.books = books;
	}

	@Override
	public void save(Book book) {
		Book foundbook = findBookById(book.getId());
		if (foundbook == null) {
			IndexOfEmptyBooksPosition = getIndexOfEmptyBooksPosition();
			if (isBookAccessibleToAddNewBook()) {
				books[IndexOfEmptyBooksPosition] = book;
			}
		}
		
		
	}

	@Override
	public Book findBookById(Integer id) {
		for (Book book : books) {
			if (bookExists(book, id)) {
				return book;
			}
		}
		return null;
	}

	@Override
	public Book update(Book book) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void before() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void after() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Integer delete(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}
	
	private Integer getIndexOfEmptyBooksPosition() {
		Integer index = 0;	
		for (Book book: books) {		
			if (book == null) {
				return index;
		}
		index++;
	}
		return index;
		
	}
	private Boolean isBookAccessibleToAddNewBook() {
		return IndexOfEmptyBooksPosition != null && IndexOfEmptyBooksPosition < books.length;
	}
	
	private Boolean bookExists(Book book, Integer id) {
		return book != null && book.getId().equals(id);
	}

}
